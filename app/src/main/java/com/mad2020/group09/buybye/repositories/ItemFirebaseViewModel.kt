package com.mad2020.group09.buybye.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.EventListener
import com.mad2020.group09.buybye.model.Item

class ItemFirebaseViewModel: ViewModel() {

    companion object {
        private const val TAG = "ITEM_FIREBASE_VIEW_MODEL"
    }

    private val itemRepository                   = ItemFirebaseRepository()
    var savedItems : MutableLiveData<List<Item>> = MutableLiveData()
    var savedItem  : MutableLiveData<Item>       = MutableLiveData()

    fun saveItemToFirebase(item: Item){
        itemRepository.saveItem(item).addOnFailureListener {
            Log.e(TAG, "SAVE FAILED on Item: ${it.message}")
        }.addOnSuccessListener {
            Log.i(TAG, "SAVE DONE on Item: item ${item.id}")
        }
    }

    fun getSavedItem(itemId: String): LiveData<Item> {
        itemRepository.getItem(itemId).addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                savedItem.value = null
                return@EventListener
            }
            val itemDb = value?.toObject(Item::class.java)
            itemDb?.id = value?.id.toString()
            savedItem.value = itemDb
        })
        return savedItem
    }

    fun getSavedItems(): LiveData<List<Item>> {
        itemRepository.getSavedItems().addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Items: ${e.message}", e)
                savedItems.value = null
                return@EventListener
            }

            Log.d(TAG, "LISTEN DONE on Items: loaded ${value!!.size()}")

            val savedItemList: MutableList<Item> = mutableListOf()
            for(doc in value) {
                val item = doc.toObject(Item::class.java)
                savedItemList.add(item)
            }
            savedItems.value = savedItemList
        })
        return savedItems
    }

    fun getSavedItemsByUser(userId: String): LiveData<List<Item>> {
        val savedItemsByUser: MutableLiveData<List<Item>> = MutableLiveData()
        itemRepository.getSavedItemsByUser(userId).addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Items: ${e.message}", e)
                savedItemsByUser.value = null
                return@EventListener
            }

            Log.d(TAG, "LISTEN DONE on Items by user: loaded ${value!!.size()}")

            val savedItemList: MutableList<Item> = mutableListOf()
            for(doc in value) {
                val item = doc.toObject(Item::class.java)
                savedItemList.add(item)
            }
            savedItemsByUser.value = savedItemList
        })
        return savedItemsByUser
    }

    fun getItemsToConfirmByUser(userId: String): LiveData<List<Item>> {
        val savedItemsByUser: MutableLiveData<List<Item>> = MutableLiveData()
        itemRepository.getItemsToConfirmByUser(userId).addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Items: ${e.message}", e)
                savedItemsByUser.value = null
                return@EventListener
            }

            Log.d(TAG, "LISTEN DONE on Items by user: loaded ${value!!.size()}")

            val savedItemList: MutableList<Item> = mutableListOf()
            for(doc in value) {
                val item = doc.toObject(Item::class.java)
                savedItemList.add(item)
            }
            savedItemsByUser.value = savedItemList
        })
        return savedItemsByUser
    }

    fun getSavedItemsByBuyer(buyerId: String): LiveData<List<Item>> {
        val savedItemsByBuyer: MutableLiveData<List<Item>> = MutableLiveData()
        itemRepository.getSavedItemsByBuyer(buyerId).addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Items: ${e.message}", e)
                savedItemsByBuyer.value = null
                return@EventListener
            }

            Log.d(TAG, "LISTEN DONE on Items by user: loaded ${value!!.size()}")

            val savedItemList: MutableList<Item> = mutableListOf()
            for(doc in value) {
                val item = doc.toObject(Item::class.java)
                savedItemList.add(item)
            }
            savedItemsByBuyer.value = savedItemList
        })
        return savedItemsByBuyer
    }

    fun getSavedItemsByIds(itemIds: List<String>): LiveData<List<Item>> {
        val savedItemsByIds: MutableLiveData<List<Item>> = MutableLiveData()
        itemRepository.getSavedItemsByIds(itemIds).addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Items: ${e.message}", e)
                savedItemsByIds.value = null
                return@EventListener
            }

            Log.d(TAG, "LISTEN DONE on Items by ids: loaded ${value!!.size()}")

            val savedItemList: MutableList<Item> = mutableListOf()
            for(doc in value) {
                val item = doc.toObject(Item::class.java)
                savedItemList.add(item)
            }
            savedItemsByIds.value = savedItemList
        })
        return savedItemsByIds
    }

    fun deleteItem(item: Item) {
        itemRepository.deleteItem(item).addOnFailureListener {
            Log.e(TAG, "DELETE FAILED on Item: ${it.message}")
        }
    }

}