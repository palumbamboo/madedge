package com.mad2020.group09.buybye.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.google.android.material.card.MaterialCardView
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.enums.ItemStatus
import com.mad2020.group09.buybye.model.Item
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.ImageRepository
import com.mad2020.group09.buybye.utilities.*
import kotlinx.android.synthetic.main.fragment_user_interested_card.view.*

class MyInterestedUserProfileAdapter(
    private val myUserDataset: LiveData<List<UserProfile>>,
    private val item: Item,
    private val lifecycleOwner: LifecycleOwner,
    private val myContextProvider: ContextProvider
) :
    LifecycleRecyclerAdapter<MyInterestedUserProfileAdapter.MyViewHolder>() {

    class MyViewHolder(private val cardView: CardView) : LifecycleViewHolder(cardView) {
        fun bindUser(
            user: UserProfile,
            item: Item,
            lifecycleOwner: LifecycleOwner,
            contextProvider: ContextProvider
        ) {

            showProgressBar()

            cardView.cardUserProfileImage.visibility = View.INVISIBLE
            cardView.cardUserProfileName.text = user.name
            cardView.cardUserProfileLocation.text = user.location
            cardView.cardUserProfileRating.rating = user.overallRating.toFloat()

            if (item.buyerId == user.id && item.status == ItemStatus.WAITING_CONFIRMATION.status) {
                cardView.setCardBackgroundColor(
                    contextProvider.getContext().getColor(R.color.toconfirm_chip)
                )
            } else if (item.buyerId == user.id && item.status == ItemStatus.SOLD.status) {
                cardView.setCardBackgroundColor(
                    contextProvider.getContext().getColor(R.color.onsale_chip)
                )
            }

            if (fileExist(user.photoId, contextProvider.getContext(), true)) {
                hideProgressBar()
                cardView.cardUserProfileImage.setImageBitmap(
                    cropAndResizeBitmap(
                        readBitmapFromStorageMemory(
                            user.photoId,
                            contextProvider.getContext()
                        )!!,
                        150
                    )
                )
                cardView.cardUserProfileImage.visibility = View.VISIBLE
            } else {
                ImageRepository().getUserImage(user.photoId).observe(lifecycleOwner, Observer {
                    hideProgressBar()
                    if (it != null) {
                        cardView.cardUserProfileImage.setImageBitmap(cropAndResizeBitmap(it, 150))
                        writeBitmapToStorageMemory(
                            it, openFile(
                                user.photoId,
                                contextProvider.getContext()
                            )
                        )
                    } else {
                        cardView.cardUserProfileImage.setImageResource(R.drawable.my_avatar)
                    }
                    cardView.cardUserProfileImage.visibility = View.VISIBLE
                })
            }
            cardView.setOnClickListener {
                it.findNavController().navigate(
                    R.id.showProfileFragment,
                    bundleOf("userGuestProfile" to user, "fromOnSale" to true)
                )
            }
        }

        private fun showProgressBar() {
            cardView.progressBarCardImage.visibility = View.VISIBLE
        }

        private fun hideProgressBar() {
            cardView.progressBarCardImage.visibility = View.INVISIBLE
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        // create a new view
        val card = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_user_interested_card, parent, false) as MaterialCardView

        return MyViewHolder(
            card
        )
    }

    override fun getItemCount(): Int {
        return myUserDataset.value!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val user: UserProfile = myUserDataset.value!![position]

        holder.bindUser(user, item, lifecycleOwner, myContextProvider)
    }
}