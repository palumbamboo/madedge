package com.mad2020.group09.buybye.enums

enum class KeyCollector(val key: String) {
    MAPS("AIzaSyDrEHScUGQjgR493Iekfylvqt23czMVvlY"),
    DIRECTIONS("AIzaSyC8V6fTZExh1xmmpWj30bVbzK0bNz3eBck"),
    GEOCODING("AIzaSyBSlNEqJJzqo5-fYwGZ9e9psgMxq4R4y7w")
}