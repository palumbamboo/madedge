package com.mad2020.group09.buybye.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*

@Keep
@Parcelize
data class Review(
    var userReviewerId: String = "",
    var userReviewedId: String = "",
    var itemReviewedId: String = "",
    var rating: Double = 0.0,
    var text: String = ""
) : Parcelable {

    @IgnoredOnParcel
    var id: String = ""

    init {
        id = UUID.randomUUID().toString()
            .replace("-", "")
            .toUpperCase(Locale.getDefault())
    }
}