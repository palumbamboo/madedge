package com.mad2020.group09.buybye.ui.maps

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.enums.KeyCollector
import kotlinx.android.synthetic.main.fragment_directions.*
import org.json.JSONObject

class DirectionsFragment : Fragment() {

    private lateinit var googleMap: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        return inflater.inflate(R.layout.fragment_directions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val startLatitude = arguments?.getDouble("currLatitude")
        val startLongitude = arguments?.getDouble("currLongitude")
        val endLatitude = arguments?.getDouble("destLatitude")
        val endLongitude = arguments?.getDouble("destLongitude")

        directionsMapView.onCreate(savedInstanceState)
        directionsMapView.onResume()
        directionsMapView.getMapAsync {
            googleMap = it
            googleMap.uiSettings.isZoomControlsEnabled = true
            // Draw the two markers
            googleMap.addMarker(
                MarkerOptions()
                    .position(
                        LatLng(
                            startLatitude!!,
                            startLongitude!!
                        )
                    )
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
            )

            googleMap.addMarker(
                MarkerOptions()
                    .position(
                        LatLng(
                            endLatitude!!,
                            endLongitude!!
                        )
                    )
            )
            // Calculate directions
            val path: MutableList<List<LatLng>> = ArrayList()
            val urlDirections =
                "https://maps.googleapis.com/maps/api/directions/json?origin=${startLatitude},${startLongitude}&destination=${endLatitude},${endLongitude}&key=${KeyCollector.DIRECTIONS.key}"
            val directionsRequest = object : StringRequest(
                Method.GET,
                urlDirections,
                Response.Listener { response ->
                    val jsonResponse = JSONObject(response)
                    // Get routes
                    val routes = jsonResponse.getJSONArray("routes")
                    val legs = routes.getJSONObject(0).getJSONArray("legs")
                    val steps = legs.getJSONObject(0).getJSONArray("steps")
                    for (i in 0 until steps.length()) {
                        val points =
                            steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                        path.add(PolyUtil.decode(points))
                    }
                    for (i in 0 until path.size) {
                        this.googleMap.addPolyline(
                            PolylineOptions().addAll(path[i]).color(Color.BLUE)
                        )
                    }
                },
                Response.ErrorListener { _ ->
                }) {}
            val requestQueue = Volley.newRequestQueue(requireContext())
            requestQueue.add(directionsRequest)

            googleMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        startLatitude,
                        startLongitude
                    ), 9.0f
                )
            )

        }
    }
}