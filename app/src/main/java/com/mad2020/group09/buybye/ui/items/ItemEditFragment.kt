package com.mad2020.group09.buybye.ui.items


import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.graphics.drawable.BitmapDrawable
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.messaging.FirebaseMessaging
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.enums.ItemDetailsStatus
import com.mad2020.group09.buybye.enums.ItemStatus
import com.mad2020.group09.buybye.enums.KeyCollector
import com.mad2020.group09.buybye.model.Item
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.ImageRepository
import com.mad2020.group09.buybye.repositories.ItemFirebaseViewModel
import com.mad2020.group09.buybye.utilities.*
import kotlinx.android.synthetic.main.fragment_item_details.*
import kotlinx.android.synthetic.main.fragment_item_edit.*
import kotlinx.android.synthetic.main.fragment_item_edit.scrollView
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.lang.Double.parseDouble
import java.util.*

class ItemEditFragment : Fragment() {

    private var buttonDate: TextView? = null
    private var calendar: Calendar = Calendar.getInstance()
    private val formatter = SimpleDateFormat(dateFormat)
    private lateinit var googleMap: GoogleMap
    private var itemLatitude: Double = 0.0
    private var itemLongitude: Double = 0.0
    private var queryLat: Double = 0.0
    private var queryLng: Double = 0.0
    private lateinit var currPosition: LatLng
    private var itemCity: String = ""
    private var itemProvince: String = ""
    private var itemCountry: String = ""


    companion object {
        //Permission code gallery
        private const val PERMISSION_CODE_GALLERY = 1000

        //image pick code
        private const val IMAGE_PICK_CODE = 1001

        //Permission code camera
        private const val PERMISSION_CODE_CAMERA = 1002

        //image capture code
        private const val IMAGE_CAPTURE_CODE = 1003

        // date format in view
        private const val dateFormat: String = "dd/MM/yyyy"

        var image_uri: Uri? = null
        var tempImageBitmap: Bitmap? = null
        var screenSize = 800
        var availableSize = 800
        var itemHasPhoto: Boolean = false
        var isNewItem = false
        var isMarkerPresent = false
    }

    lateinit var item: Item
    lateinit var itemImage: Bitmap
    lateinit var userProfile: UserProfile

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        return inflater.inflate(R.layout.fragment_item_edit, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.edit_item_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.save_item -> {
                saveItem()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putCharSequence("category", filled_exposed_dropdown_cat.text.toString())
        outState.putCharSequence("subcategory", filled_exposed_dropdown_subcat.text.toString())
        outState.putCharSequence("expiryDate", buttonDate?.text.toString())
        outState.putDouble("itemLatitude", itemLatitude)
        outState.putDouble("itemLongitude", itemLongitude)

        tempImageBitmap?.let {
            val stream = ByteArrayOutputStream()
            it.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val byteArray: ByteArray = stream.toByteArray()
            outState.putByteArray("tempImageBitmap", byteArray)
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        initCategoriesAndSubcategories(requireView())

        setSubcategory(resources.getStringArray(R.array.Categories))

        savedInstanceState?.let {
            buttonDate?.text = savedInstanceState.getCharSequence("expiryDate")

            filled_exposed_dropdown_cat.setText(
                savedInstanceState.getCharSequence("category"),
                false
            )
            filled_exposed_dropdown_subcat.setText(
                savedInstanceState.getCharSequence("subcategory"),
                false
            )

            savedInstanceState.getByteArray("tempImageBitmap")?.let { image ->
                tempImageBitmap = BitmapFactory.decodeByteArray(image, 0, image.size)
                itemEditPhoto.setImageBitmap(cropAndResizeBitmap(tempImageBitmap!!, availableSize))
            }

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initScrollviewBehavior()

        context?.let {
            screenSize = screenMaxSize(it)
            availableSize = screenAvailableSize(it)
        }

        tempImageBitmap = null
        itemHasPhoto = false
        isNewItem = false

        // UI customization
        buttonDate = getView()?.findViewById(R.id.itemEditExpiryDateButton)

        buttonDate?.text = formatter.format(System.currentTimeMillis() + 86400000)

        // create an OnDateSetListener
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        buttonDate!!.setOnClickListener {
            activity?.let {
                val dpd = DatePickerDialog(
                    it,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
                )
                dpd.datePicker.minDate = System.currentTimeMillis()
                dpd.show()
            }
        }

        // Retrieve item from db if present, otherwise item

        val passedItem: Item? =
            arguments?.getParcelable("item") // we could pass only the id instead of the item
        userProfile = arguments?.getParcelable("userProfileItem")!!

        if (passedItem == null) {
            isNewItem = true
            item = Item()
        } else {
            item = passedItem
            arguments?.getBoolean("itemImage")?.let {
                if (it)
                    populateXML(item, readBitmapFromStorageMemory(item.photoId, requireContext())!!)
                else
                    populateXML(item, null)
            }
        }

        // Map initialization
        itemEditMapView.onCreate(savedInstanceState)
        itemEditMapView.onResume()
        itemEditMapView.getMapAsync {
            googleMap = it
            googleMap.uiSettings.isZoomControlsEnabled = true
            // Ask for location permissions
            while (ContextCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions()
            }
            googleMap.isMyLocationEnabled = true

            // Check for updated marker after screen rotation
            val passedLatitude = savedInstanceState?.getDouble("itemLatitude")
            val passedLongitude = savedInstanceState?.getDouble("itemLongitude")
            if (passedLatitude != null && passedLongitude != null && passedLatitude != 0.0 && passedLongitude != 0.0) {
                itemLatitude = passedLatitude
                itemLongitude = passedLongitude
            }

            // Place search
            itemEditSearchLocation.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }

                override fun onQueryTextSubmit(query: String?): Boolean {
                    var cleanQuery = query?.replace(" ", "%20")
                    val urlLocation =
                        "https://maps.googleapis.com/maps/api/geocode/json?address=${cleanQuery}&key=${KeyCollector.GEOCODING.key}"
                    val locationRequest = object : StringRequest(
                        Request.Method.GET,
                        urlLocation,
                        Response.Listener<String> { response ->
                            val jsonResponse = JSONObject(response)
                            // Get routes
                            val results = jsonResponse.getJSONArray("results")
                            val geometry = results.getJSONObject(0).getJSONObject("geometry")
                            val location = geometry.getJSONObject("location")
                            queryLat = location.getDouble("lat")
                            queryLng = location.getDouble("lng")
                            if (queryLat != 0.0 && queryLng != 0.0) {
                                googleMap.moveCamera(
                                    CameraUpdateFactory.newLatLngZoom(
                                        LatLng(
                                            queryLat,
                                            queryLng
                                        ), 12.0f
                                    )
                                )
                            }

                        },
                        Response.ErrorListener { _ ->
                        }) {}
                    val requestQueue = Volley.newRequestQueue(requireContext())
                    requestQueue.add(locationRequest)
                    return false
                }
            })

            // Retrieve location's coordinates, and move camera there if a position has not already been marked
            FusedLocationProviderClient(requireActivity()).lastLocation.addOnCompleteListener { loc ->
                currPosition = LatLng(loc.result!!.latitude, loc.result!!.longitude)

                if (!isMarkerPresent) {
                    googleMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            currPosition,
                            16.0f
                        )
                    ) // Moving map to position
                }
            }

            // A marker was already assigned to the item, checking also that latitude and longitude are not null for the previous elements already stored on Firestore
            if (itemLatitude != 0.0 && itemLongitude != 0.0) {
                googleMap.addMarker(MarkerOptions().position(LatLng(itemLatitude, itemLongitude)))
                googleMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            itemLatitude,
                            itemLongitude
                        ), 16.0f
                    )
                ) // Moving map to position
                isMarkerPresent = true
            } else if (item.latitude != 0.0 && item.longitude != 0.0) {
                googleMap.addMarker(MarkerOptions().position(LatLng(item.latitude, item.longitude)))
                googleMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            item.latitude,
                            item.longitude
                        ), 16.0f
                    )
                ) // Moving map to position
                isMarkerPresent = true
                itemLatitude = item.latitude
                itemLongitude = item.longitude
            }

            // Marker management
            googleMap.setOnMapClickListener { pos ->
                val markerOptions = MarkerOptions()
                markerOptions.position(pos)
                if (!isMarkerPresent) {
                    //No marker yet in the map
                    googleMap.addMarker(markerOptions)
                    googleMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            pos,
                            16.0f
                        )
                    ) // Moving map to marker
                    isMarkerPresent = true
                } else {
                    // Delete old marker
                    googleMap.clear()
                    googleMap.addMarker(markerOptions)
                    googleMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            pos,
                            16.0f
                        )
                    ) // Moving map to marker
                }
                itemLatitude = markerOptions.position.latitude
                itemLongitude = markerOptions.position.longitude

                // Fetch city and country
                val urlDirections =
                    "https://maps.googleapis.com/maps/api/geocode/json?latlng=${itemLatitude},${itemLongitude}&key=${KeyCollector.GEOCODING.key}"
                val directionsRequest = object : StringRequest(
                    Request.Method.GET,
                    urlDirections,
                    Response.Listener<String> { response ->
                        val jsonResponse = JSONObject(response)
                        // Get routes
                        val results = jsonResponse.getJSONArray("results")
                        val addressComponents =
                            results.getJSONObject(0).getJSONArray("address_components")
                        val size = addressComponents.length()
                        itemCity = if (size >= 3) addressComponents.getJSONObject(2)
                            .getString("short_name") else "No Data"
                        itemProvince = if (size >= 5) addressComponents.getJSONObject(4)
                            .getString("short_name") else "No Data"
                        itemCountry = if (size >= 7) addressComponents.getJSONObject(6)
                            .getString("long_name") else "No Data"
                        var isPostalCode = true
                        try {
                            val num = parseDouble(itemCountry)
                        } catch (e: NumberFormatException) {
                            isPostalCode = false
                        }
                        if(isPostalCode){
                            itemCountry = if (size >= 7) addressComponents.getJSONObject(5)
                                .getString("long_name") else "No Data"
                        }


                    },
                    Response.ErrorListener { _ ->
                    }) {}
                val requestQueue = Volley.newRequestQueue(requireContext())
                requestQueue.add(directionsRequest)
            }
        }


        // set click listener on rotate button
        itemEditImageRotateButton.setOnClickListener {
            if (tempImageBitmap == null && !itemHasPhoto) {
                Toast.makeText(
                    requireContext(),
                    "Please upload a photo to rotate it.",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (tempImageBitmap != null) {
                tempImageBitmap = tempImageBitmap!!.rotate(90)
                itemEditPhoto.setImageBitmap(cropAndResizeBitmap(tempImageBitmap!!, availableSize))
            } else {
                tempImageBitmap =
                    readBitmapFromStorageMemory(item.photoId, requireActivity().applicationContext)
                tempImageBitmap = tempImageBitmap!!.rotate(90)
                itemEditPhoto.setImageBitmap(cropAndResizeBitmap(tempImageBitmap!!, availableSize))
            }
        }

        // set click listener on edit photo button
        itemEditImageButton.setOnClickListener { v ->
            val builder = v?.context?.let {
                AlertDialog.Builder(it)
            }

            builder?.setTitle("Select a new image")
            builder?.setItems(
                arrayOf("from Camera", "from Gallery")
            ) { _, which ->
                if (which == 0) {
                    if (requireActivity().checkSelfPermission(Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED ||
                        requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED
                    ) {
                        //permission was not enabled
                        val permission = arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        //show popup to request permission
                        requestPermissions(permission, PERMISSION_CODE_CAMERA)
                    } else {
                        pickImageFromCamera()
                    }
                } else if (which == 1) {
                    if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_DENIED
                    ) {
                        //permission denied
                        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                        //show popup to request runtime permission
                        requestPermissions(permissions, PERMISSION_CODE_GALLERY)
                    } else {
                        //permission already granted
                        pickImageFromGallery()
                    }
                }
            }
            builder?.create()?.show()
        }

    }

    private fun populateXML(item: Item, image: Bitmap?) {

        itemEditTitle.setText(item.title)
        itemEditDescription.setText(item.description)
        itemEditPrice.setText(item.price.toString())
        itemEditSearchLocation.setQuery(item.location, false);

        filled_exposed_dropdown_cat.setText(item.category, false)
        filled_exposed_dropdown_subcat.setText(item.subcategory, false)
        filled_exposed_dropdown_status.setText(item.status, false)

        itemEditExpiryDateButton.text = formatter.format(item.expiryDate)

        image?.let {
            itemImage = cropAndResizeBitmap(it, availableSize)
            itemEditPhoto.setImageBitmap(itemImage)
            itemHasPhoto = true
        }

    }

    private fun initCategoriesAndSubcategories(view: View): Pair<AutoCompleteTextView, AutoCompleteTextView> {
        val categories = resources.getStringArray(R.array.Categories)
        val subcategories = arrayOf<String>()
        val status = arrayOf(ItemStatus.ON_SALE.status, ItemStatus.BLOCKED.status)

        val adapterSubcat = activity?.let {
            ArrayAdapter(
                it,
                R.layout.dropdown_menu_popup_item_subcat,
                subcategories
            )
        }

        val editTextFilledExposedDropdownSubcat: AutoCompleteTextView =
            view.findViewById(R.id.filled_exposed_dropdown_subcat)

        editTextFilledExposedDropdownSubcat.setAdapter(adapterSubcat)

        editTextFilledExposedDropdownSubcat.setOnClickListener {
            if ((it as AppCompatAutoCompleteTextView).adapter.isEmpty) {
                Toast.makeText(context, "Please first select a category.", Toast.LENGTH_SHORT)
                    .show()
            } else {
                (it as AutoCompleteTextView).showDropDown()
            }
        }

        val adapterStatus = activity?.let {
            ArrayAdapter(
                it,
                R.layout.dropdown_menu_popup_item,
                status
            )
        }

        val editTextFilledExposedDropdownLayout: AutoCompleteTextView =
            view.findViewById(R.id.filled_exposed_dropdown_status)

        editTextFilledExposedDropdownLayout.setAdapter(adapterStatus)

        val adapterCat = activity?.let {
            ArrayAdapter(
                it,
                R.layout.dropdown_menu_popup_item,
                categories
            )
        }
        val editTextFilledExposedDropdownCat: AutoCompleteTextView =
            view.findViewById(R.id.filled_exposed_dropdown_cat)

        editTextFilledExposedDropdownCat.setAdapter(adapterCat)

        editTextFilledExposedDropdownCat.setOnClickListener {
            (it as AutoCompleteTextView).showDropDown()
        }

        editTextFilledExposedDropdownCat.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                editTextFilledExposedDropdownSubcat.setText("Select subcategory", false)

                setSubcategory(categories)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}
        })
        return Pair(editTextFilledExposedDropdownSubcat, editTextFilledExposedDropdownCat)
    }

    // other methods to manage fragment logic

    private fun updateDateInView() {
        val sdf = SimpleDateFormat(dateFormat, Locale.US)
        buttonDate!!.text = sdf.format(calendar.time)
    }

    /**
     * Private method to return data to ShowActivity
     */
    private fun saveItem() {
        val itemRepo = ViewModelProvider(this).get(ItemFirebaseViewModel::class.java)
        if (itemEditTitle.text.isNotEmpty() &&
            itemEditDescription.text.isNotEmpty() &&
            itemEditPrice.text.isNotEmpty() &&
            filled_exposed_dropdown_cat.text.isNotEmpty() &&
            filled_exposed_dropdown_subcat.text.isNotEmpty() &&
            arrayListOf(ItemStatus.BLOCKED.status, ItemStatus.ON_SALE.status).contains(
                filled_exposed_dropdown_status.text.toString()
            ) &&
            itemLatitude != 0.0 &&
            itemLongitude != 0.0
        ) {
            item.title = itemEditTitle.text.toString()
            item.description = itemEditDescription.text.toString()
            item.price = itemEditPrice.text.toString().toDouble()
            item.category = filled_exposed_dropdown_cat.text.toString()
            item.subcategory = filled_exposed_dropdown_subcat.text.toString()
            item.status = filled_exposed_dropdown_status.text.toString()
            // Basic check to see if the user has put a marker, could be improved
            if (isMarkerPresent) {
                item.latitude = itemLatitude
                item.longitude = itemLongitude
            }
            if (itemCity.isNotEmpty() && itemProvince.isNotEmpty() && itemCountry.isNotEmpty()) {
                item.location = "${itemCity} (${itemProvince})"
                item.country = itemCountry
            }

            item.sellerId = userProfile.id
            item.sellerEmail = userProfile.email
            val parser = SimpleDateFormat(dateFormat)
            item.expiryDate = parser.parse(itemEditExpiryDateButton.text.toString())

            val bundle = Bundle()
            bundle.putParcelable("userProfileItemUpdated", userProfile)

            itemRepo.saveItemToFirebase(item)
            bundle.putParcelable("itemUpdated", item)
            bundle.putString("itemDetailStatus", ItemDetailsStatus.FROM_EDIT.status)

            tempImageBitmap?.let { bitmap ->
                ImageRepository().uploadItemImage(item.photoId, bitmap)
                writeBitmapToStorageMemory(bitmap, openFile(item.photoId, requireActivity()))
                bundle.putBoolean("itemImageUpdated", true)
            }

            FirebaseMessaging.getInstance().subscribeToTopic(item.id + "-interest")
                .addOnCompleteListener { task ->
                    var msg = "Push ok"
                    if (!task.isSuccessful) {
                        msg = "Push error"
                    }
                    Log.d("subscribe", msg)
                }

            val imm =
                requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

            val focusedView = requireActivity().currentFocus
            // If no view is focused, an NPE will be thrown

            if (focusedView != null) {
                imm.hideSoftInputFromWindow(requireView().windowToken, 0)
            }

            findNavController().popBackStack()
        } else {
            Toast.makeText(
                context,
                "Some fields are missing. Please insert them.",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    /**
     * invoke an intent to a camera app to retrieve a new image
     */
    private fun pickImageFromCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera")
        image_uri = requireActivity().contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    /**
     * invoke an intent to a photo gallery app to retrieve a saved image
     */
    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    /**
     * handle requested permission result
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE_CAMERA -> {
                if (grantResults.isEmpty() ||
                    grantResults[0] != PackageManager.PERMISSION_GRANTED ||
                    grantResults[1] != PackageManager.PERMISSION_GRANTED
                ) {
                    Toast.makeText(
                        requireActivity().applicationContext,
                        "Permission denied",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    pickImageFromCamera()
                }
            }
            PERMISSION_CODE_GALLERY -> {
                if (grantResults.isEmpty() ||
                    grantResults[0] != PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup denied
                    Toast.makeText(
                        requireActivity().applicationContext,
                        "Permission denied",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    pickImageFromGallery()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var newBitmap: Bitmap? = null

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_CAPTURE_CODE) {
            Toast.makeText(
                requireActivity().applicationContext,
                "Item image updated",
                Toast.LENGTH_SHORT
            ).show()
            newBitmap = if (Build.VERSION.SDK_INT < 28) {
                MediaStore.Images.Media.getBitmap(
                    requireActivity().contentResolver,
                    image_uri
                )
            } else {
                val source = image_uri?.let {
                    ImageDecoder.createSource(
                        requireActivity().contentResolver,
                        it
                    )
                }
                source?.let { ImageDecoder.decodeBitmap(it) }
            }
        }

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            Toast.makeText(
                requireActivity().applicationContext,
                "Item image updated",
                Toast.LENGTH_SHORT
            ).show()
            val imageView = ImageView(requireActivity().applicationContext)
            imageView.setImageURI(data?.data)
            newBitmap = (imageView.drawable as BitmapDrawable).bitmap
        }

        tempImageBitmap = newBitmap?.let { cropAndResizeBitmap(it, screenSize) }
        tempImageBitmap?.let {
            itemEditPhoto.setImageBitmap(
                cropAndResizeBitmap(
                    it,
                    availableSize
                )
            )
        }
    }

    private fun setSubcategory(categories: Array<String>) {
        val index = categories.indexOf(filled_exposed_dropdown_cat.text.toString())
        if (index >= 0) {
            val id = requireContext().resources.getIdentifier(
                "Category${index + 1}",
                "array",
                requireContext().packageName
            )
            val subcat = resources.getStringArray(id)

            val newAdapter = activity?.let {
                ArrayAdapter(
                    it,
                    R.layout.dropdown_menu_popup_item_subcat,
                    subcat
                )
            }
            filled_exposed_dropdown_subcat.setAdapter(newAdapter)
        }
    }

    /*
        set scrollview event and description scroll event to avoid conflicts
    */
    private fun initScrollviewBehavior() {
        itemEditDescription.movementMethod = ScrollingMovementMethod()

        scrollView.setOnTouchListener { v, _ ->
            v.performClick()
            itemEditDescription.parent.requestDisallowInterceptTouchEvent(false)
            false
        }

        itemEditDescription.setOnTouchListener { v, event ->
            v.performClick()
            val descriptionView = (v as EditText)
            if (descriptionView.lineCount > descriptionView.maxLines) {
                itemEditDescription.parent.requestDisallowInterceptTouchEvent(true)
            }
            v.onTouchEvent(event)
        }

        itemEditMapViewTransparent.setOnTouchListener { view, event ->
            view.performClick()
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    scrollView.requestDisallowInterceptTouchEvent(true)
                    false
                }
                MotionEvent.ACTION_UP -> {
                    scrollView.requestDisallowInterceptTouchEvent(false)
                    true
                }
                MotionEvent.ACTION_MOVE -> {
                    scrollView.requestDisallowInterceptTouchEvent(true)
                    false
                }
                else -> true
            }
        }

        itemEditDescription.setSelectAllOnFocus(true)
        itemEditDescription.requestFocus()
    }

    /*
        Request location permissions to user
     */
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            42
        )
        // Request code 42 is for the location
    }

}