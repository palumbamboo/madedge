package com.mad2020.group09.buybye.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*

@Keep
@Parcelize
data class Interest(
    var itemId: String = "",
    var userId: String = ""
) : Parcelable {
    @IgnoredOnParcel
    var id: String = ""

    init {
        if (this.id == "") {
            id = UUID.randomUUID().toString()
                .replace("-", "")
                .toUpperCase(java.util.Locale.getDefault())
        }
    }

}