package com.mad2020.group09.buybye.ui.items

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.os.Parcelable
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.textview.MaterialTextView
import com.google.firebase.messaging.FirebaseMessaging
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.adapters.ContextProvider
import com.mad2020.group09.buybye.adapters.MyInterestedUserProfileAdapter
import com.mad2020.group09.buybye.enums.ItemDetailsStatus
import com.mad2020.group09.buybye.enums.ItemStatus
import com.mad2020.group09.buybye.model.Interest
import com.mad2020.group09.buybye.model.Item
import com.mad2020.group09.buybye.model.Review
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.*
import com.mad2020.group09.buybye.ui.dialogs.BuyerChooserDialog
import com.mad2020.group09.buybye.utilities.*
import kotlinx.android.synthetic.main.fragment_item_details.*
import java.util.*


class ItemDetailsFragment : Fragment(), BuyerChooserDialog.DialogListener {

    private lateinit var itemReceived: Item
    private lateinit var itemImage: Bitmap
    private lateinit var editItemButton: MenuItem
    private lateinit var userProfileItem: UserProfile
    private lateinit var userGuest: UserProfile
    private var availableSize = 800

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyInterestedUserProfileAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var userInterested = MutableLiveData<List<UserProfile>>()
    private lateinit var users: List<UserProfile>

    lateinit var itemDetailsStatus: String
    private val BUYER_DIALOG_CODE = 500
    private lateinit var userBuyer: UserProfile
    private lateinit var userSeller: UserProfile

    private lateinit var googleMap: GoogleMap
    private lateinit var currPosition: LatLng


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_details, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        val calleeBundle: Bundle? =
            this.arguments //Retrieve the callee from the bundle and check its value -> if true, no edit should be inflated
        if (calleeBundle != null) {
            val itemStatus: String = calleeBundle.getString("itemDetailStatus")!!
            if (itemStatus == ItemDetailsStatus.FROM_PERSONAL.status || itemStatus == ItemDetailsStatus.FROM_EDIT.status) {
                inflater.inflate(R.menu.buybye_menu, menu)
                editItemButton = menu.findItem(R.id.edit_item)
            }

            if (itemStatus != ItemDetailsStatus.FROM_ON_SALE.status) {
                fabInterest.visibility = View.INVISIBLE
                fabNotInterest.visibility = View.INVISIBLE
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.edit_item -> {
                editItem()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initScrollviewBehavior()

        hideViewsForLoading()
        showProgressBar()

        itemDetailsStatus = requireArguments().getString("itemDetailStatus")!!

        availableSize = screenAvailableSize(requireContext())
        val itemUpdated: Item? = arguments?.getParcelable("itemUpdated")

        val itemPassed: Item? = if (itemUpdated == null) {
            arguments?.getParcelable("item")
        } else {
            null
        }

        itemReceived = itemUpdated ?: itemPassed!!

        when (itemDetailsStatus) {
            ItemDetailsStatus.FROM_ON_SALE.status -> {
                ImageRepository().getItemImage(itemReceived.photoId)
                    .observe(viewLifecycleOwner, Observer { image ->
                        if (userExist()) {
                            getCurrentUser().observe(viewLifecycleOwner, Observer { userGuestDb ->
                                InterestViewModel().checkUserInterest(
                                    itemReceived.id,
                                    userGuestDb.id
                                )
                                    .observe(viewLifecycleOwner, Observer { result ->
                                        if (result == null) {
                                            // No interest
                                            fabInterest.visibility = View.VISIBLE
                                            fabNotInterest.visibility = View.INVISIBLE
                                        } else {
                                            fabNotInterest.visibility = View.VISIBLE
                                            fabInterest.visibility = View.INVISIBLE
                                        }
                                        userGuest = userGuestDb
                                        populateXML(itemReceived, image, null)
                                    })
                            })
                        } else {
                            populateXML(itemReceived, image, null)
                        }
                    })
            }
            // from my ItemOnSaleAdapter
            ItemDetailsStatus.FROM_PERSONAL.status -> {
                val userPassed: UserProfile = requireArguments().getParcelable("userPassed")!!

                ImageRepository().getItemImage(itemReceived.photoId)
                    .observe(viewLifecycleOwner, Observer { image ->
                        loadItemStatus()
                        loadInterestedUser(itemReceived)
                        populateXML(itemReceived, image, userPassed)
                    })
            }
            ItemDetailsStatus.FROM_EDIT.status -> {
                val updatedUser: UserProfile =
                    requireArguments().getParcelable("userProfileItemUpdated")!!

                if (arguments?.containsKey("itemImageUpdated") == true) {
                    val imageUpdated =
                        readBitmapFromStorageMemory(itemReceived.photoId, requireContext())
                    loadItemStatus()
                    loadInterestedUser(itemReceived)
                    populateXML(itemReceived, imageUpdated, updatedUser)
                } else {
                    ImageRepository().getItemImage(itemReceived.photoId)
                        .observe(viewLifecycleOwner, Observer { itemImage ->
                            loadItemStatus()
                            loadInterestedUser(itemReceived)
                            populateXML(itemReceived, itemImage, updatedUser)
                        })
                }

            }
            ItemDetailsStatus.FROM_INTEREST.status -> {
                val imageIsPresent: Boolean? = requireArguments().getBoolean("itemImage")
                userGuest = requireArguments().getParcelable("interestedUser")!!

                if (imageIsPresent != null && imageIsPresent) {
                    val image =
                        readBitmapFromStorageMemory(itemReceived.photoId, requireContext())
                    fabNotInterest.visibility = View.VISIBLE
                    fabInterest.visibility = View.INVISIBLE
                    populateXML(itemReceived, image, null)
                }
                ImageRepository().getItemImage(itemReceived.photoId)
                    .observe(viewLifecycleOwner, Observer { image ->
                        fabNotInterest.visibility = View.VISIBLE
                        fabInterest.visibility = View.INVISIBLE
                        populateXML(itemReceived, image, null)
                    })
            }
            ItemDetailsStatus.FROM_BOUGHT.status -> {
                val imageIsPresent: Boolean? = requireArguments().getBoolean("itemImage")
                userGuest = requireArguments().getParcelable("interestedUser")!!
                UserFirebaseViewModel().getSavedUser(itemReceived.sellerId)
                    .observe(viewLifecycleOwner, Observer {
                        userSeller = it!!
                    })

                ImageRepository().getItemImage(itemReceived.photoId)
                    .observe(viewLifecycleOwner, Observer { image ->
                        fabNotInterest.visibility = View.INVISIBLE
                        fabInterest.visibility = View.INVISIBLE
                        populateXML(itemReceived, image, null)
                        loadItemStatus()
                        loadUserReview()
                    })
            }
            ItemDetailsStatus.FROM_BOUGHT_CONFIRMATION.status -> {
                val imageIsPresent: Boolean? = requireArguments().getBoolean("itemImage")
                userGuest = requireArguments().getParcelable("interestedUser")!!
                UserFirebaseViewModel().getSavedUser(itemReceived.sellerId)
                    .observe(viewLifecycleOwner, Observer {
                        userSeller = it!!
                    })

                ImageRepository().getItemImage(itemReceived.photoId)
                    .observe(viewLifecycleOwner, Observer { image ->
                        fabInterest.visibility = View.INVISIBLE
                        fabNotInterest.visibility = View.INVISIBLE
                        fabConfirm.visibility = View.VISIBLE
                        fabReject.visibility = View.VISIBLE
                        populateXML(itemReceived, image, null)
                        loadItemStatus()
                    })

            }
        }


        if (itemReceived.latitude != 0.0 && itemReceived.longitude != 0.0) {
            // Map management
            itemDetailsMapView.visibility = View.VISIBLE
            itemDetailsMapViewTransparent.visibility = View.VISIBLE
            itemDetailsMapView.onCreate(savedInstanceState)
            itemDetailsMapView.onResume()
            itemDetailsMapView.getMapAsync {
                googleMap = it
                googleMap.uiSettings.isZoomControlsEnabled = true
                googleMap.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            itemReceived.latitude,
                            itemReceived.longitude
                        )
                    )
                )
                googleMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            itemReceived.latitude,
                            itemReceived.longitude
                        ), 16.0f
                    )
                ) // Moving map to the item's location

                // Ask for current location permissions
                while (ContextCompat.checkSelfPermission(
                        requireActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions()
                }
                googleMap.isMyLocationEnabled = true

                FusedLocationProviderClient(requireActivity()).lastLocation.addOnCompleteListener { loc ->
                    currPosition = LatLng(loc.result!!.latitude, loc.result!!.longitude)
                    itemDetailsDirections.setOnClickListener {
                        // Open new fragment with other map and directions
                        var locationBundle = Bundle()
                        locationBundle.putDouble("currLatitude", currPosition.latitude)
                        locationBundle.putDouble("currLongitude", currPosition.longitude)
                        locationBundle.putDouble("destLatitude", itemReceived.latitude)
                        locationBundle.putDouble("destLongitude", itemReceived.longitude)
                        findNavController().navigate(
                            R.id.action_itemDetailsFragment_to_directionsFragment,
                            locationBundle
                        )
                    }
                }
            }
        }

        fabInterest.setOnClickListener {
            if (this::userGuest.isInitialized) {
                fabInterest.visibility = View.INVISIBLE
                fabNotInterest.isEnabled = false
                fabNotInterest.visibility = View.VISIBLE
                InterestViewModel().saveInterestToFirebase(Interest(itemReceived.id, userGuest.id))
                FirebaseMessaging.getInstance().subscribeToTopic(itemReceived.id)
                    .addOnCompleteListener { task ->
                        var msg = "New interest!"
                        if (!task.isSuccessful) {
                            msg = "Interest not added"
                        }
                        Log.d("subscribe", msg + " id: " + itemReceived.id)
                        if (fabNotInterest != null) {
                            fabNotInterest.isEnabled = true
                            Toast.makeText(
                                requireContext(),
                                "You are now following this product!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } else {
                Toast.makeText(requireContext(), "Operation denied", Toast.LENGTH_SHORT).show()
            }
        }

        fabConfirm.setOnClickListener {
            if (this::userGuest.isInitialized) {
                fabConfirm.visibility = View.INVISIBLE
                fabReject.visibility = View.INVISIBLE

                itemReceived.status = ItemStatus.SOLD.status
                itemReceived.boughtDate = Date()
                itemReceived.editable = false
                ItemFirebaseRepository().saveItem(itemReceived)

                val navController = requireView().findNavController()

                navController.popBackStack()
                navController.navigate(R.id.nav_boughtItemsList)
                navController.navigate(
                    R.id.itemDetailsFragment, bundleOf(
                        "item" to itemReceived,
                        "itemDetailStatus" to ItemDetailsStatus.FROM_BOUGHT.status,
                        "itemImage" to fileExist(itemReceived.photoId, requireContext(), true),
                        "interestedUser" to userGuest
                    )
                )

                // This callback will only be called when MyFragment is at least Started.
                requireActivity().onBackPressedDispatcher.addCallback(this) {

                }

                Toast.makeText(requireContext(), "Purchase confirmed!", Toast.LENGTH_SHORT).show()

            } else {
                Toast.makeText(requireContext(), "Operation denied", Toast.LENGTH_SHORT).show()
            }
        }

        fabReject.setOnClickListener {
            if (this::userGuest.isInitialized) {
                fabConfirm.visibility = View.INVISIBLE
                fabReject.visibility = View.INVISIBLE

                itemReceived.buyerId = null
                itemReceived.buyerEmail = null
                itemReceived.status = ItemStatus.ON_SALE.status
                itemReceived.editable = true
                ItemFirebaseRepository().saveItem(itemReceived)
                InterestViewModel().deleteAssociatedInterest(itemReceived.id, userGuest.id)

                val navController = requireView().findNavController()

                navController.popBackStack()
                navController.navigate(R.id.nav_itemsToConfirm)

                Toast.makeText(requireContext(), "Purchase rejected", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), "Operation denied", Toast.LENGTH_SHORT).show()
            }
        }

        // Not interested in
        fabNotInterest.setOnClickListener {
            if (this::userGuest.isInitialized) {
                InterestViewModel().deleteAssociatedInterest(itemReceived.id, userGuest.id)
                fabInterest.visibility = View.VISIBLE
                fabNotInterest.visibility = View.INVISIBLE
                Toast.makeText(
                    requireContext(),
                    "You are no longer following this product!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        chooseBuyerButton.setOnClickListener {
            val fragmentTransaction = parentFragmentManager.beginTransaction()
            val prev = parentFragmentManager.findFragmentByTag("dialog")
            if (prev != null) {
                fragmentTransaction.remove(prev)
            }

            val chooseBuyerFragment = BuyerChooserDialog()
            chooseBuyerFragment.setTargetFragment(this, BUYER_DIALOG_CODE)
            var bundle = Bundle()
            if (users.isNotEmpty()) {
                val userList = arrayListOf<Parcelable>()
                for (user in users) {
                    userList.add(user)
                }
                bundle.putParcelableArrayList("users", userList)
            }
            chooseBuyerFragment.arguments = bundle
            chooseBuyerFragment.show(fragmentTransaction, "dialog")
        }
    }

    private fun populateXML(
        item: Item,
        image: Bitmap?,
        user: UserProfile?
    ) {

        itemDetailTitle.text = item.title
        itemDetailPrice.text = item.price.toString()
        itemDetailDescription.text = item.description
        itemDetailCategory.text = item.category
        itemDetailSubcategory.text = item.subcategory
        itemDetailLocation.text = item.location

        if (item.location.isNotEmpty()) {
            itemDetailLocation.text = item.location
        }

        if (item.country.isNotEmpty()) {
            itemDetailLocationState.text = item.country
        }
        val formatter = SimpleDateFormat("dd/MM/yyyy")
        itemDetailCreationDate.text = formatter.format(item.creationDate)
        itemDetailExpiryDate.text = formatter.format(item.expiryDate)

        if (user != null)
            userProfileItem = user

        if (image != null) {
            itemImage = cropAndResizeBitmap(image, availableSize)
            itemDetailPhoto.setImageBitmap(itemImage)

            if (!fileExist(itemReceived.photoId, requireContext(), true))
                DoAsync {
                    writeBitmapToStorageMemory(
                        itemImage,
                        openFile(itemReceived.photoId, requireActivity())
                    )
                }.execute()
        }

        if (itemDetailsStatus != ItemDetailsStatus.FROM_PERSONAL.status && itemDetailsStatus != ItemDetailsStatus.FROM_EDIT.status) {
            val chipStatus: View? = requireView().findViewById(R.id.itemDetailStatus)
            if (chipStatus != null)
                (chipStatus.parent as ViewGroup).removeView(chipStatus)
        }

        hideProgressBar()
        restoreViewsAfterLoading()
    }

    private fun loadUserReview() {
        ReviewViewModel().getReviewByUserAndItem(itemReceived.buyerId!!, itemReceived.id)
            .observe(viewLifecycleOwner, Observer {
                if (it == null) {
                    itemDetailsUserReviewLayout.visibility = View.VISIBLE
                    itemDetailsUserReviewButton.setOnClickListener {
                        if (itemDetailsUserReviewRating.rating == 0.0F) {
                            Toast.makeText(
                                requireContext(),
                                "Please give a rating before submitting!",
                                Toast.LENGTH_SHORT
                            ).show()
                            return@setOnClickListener
                        } else {
                            val newReview = Review()
                            newReview.userReviewerId = itemReceived.buyerId!!
                            newReview.userReviewedId = itemReceived.sellerId
                            newReview.itemReviewedId = itemReceived.id
                            newReview.rating = itemDetailsUserReviewRating.rating.toDouble()
                            newReview.text = itemDetailsUserReviewText.text.toString()
                            ReviewViewModel().saveReviewToFirebase(newReview)
                            Toast.makeText(
                                requireContext(),
                                "Your review was saved!",
                                Toast.LENGTH_SHORT
                            ).show()
                            userSeller.addNewRating(newReview.rating)
                            UserFirebaseViewModel().saveUserToFirebase(userSeller)
                            itemDetailsUserReviewLayout.visibility = View.VISIBLE
                            itemDetailsUserReviewReviewedIt.visibility = View.VISIBLE
                            itemDetailsUserReviewButton.visibility = View.INVISIBLE
                            itemDetailsUserReviewIntro.visibility = View.INVISIBLE
                            itemDetailsUserReviewRating.visibility = View.INVISIBLE
                            itemDetailsUserReviewText.visibility = View.INVISIBLE
                        }

                    }
                } else {
                    itemDetailsUserReviewLayout.visibility = View.VISIBLE
                    itemDetailsUserReviewReviewedIt.visibility = View.VISIBLE
                    itemDetailsUserReviewButton.visibility = View.INVISIBLE
                    itemDetailsUserReviewIntro.visibility = View.INVISIBLE
                    itemDetailsUserReviewRating.visibility = View.INVISIBLE
                    itemDetailsUserReviewText.visibility = View.INVISIBLE
                }
            })
    }

    private fun loadItemStatus() {
        itemDetailStatus.text = itemReceived.status
        when (itemReceived.status) {
            ItemStatus.BLOCKED.status -> {
                itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.blocked_chip
                    )
                )
                itemDetailStatus.setTextColor(Color.BLACK)
            }
            ItemStatus.ON_SALE.status -> {
                itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.onsale_chip
                    )
                )
                itemDetailStatus.setTextColor(Color.BLACK)
            }
            ItemStatus.SOLD.status -> {
                itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.sold_chip
                    )
                )
                itemDetailStatus.setTextColor(Color.WHITE)
            }
            ItemStatus.WAITING_CONFIRMATION.status -> {
                itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.toconfirm_chip
                    )
                )
                itemDetailStatus.setTextColor(Color.BLACK)
            }
        }
        itemDetailStatus.visibility = View.VISIBLE
    }

    /*
        set scrollview event and description scroll event to avoid conflicts
     */
    private fun initScrollviewBehavior() {

        itemDetailDescription.movementMethod = ScrollingMovementMethod()

        scrollView.setOnTouchListener { view, _ ->
            view.performClick()
            itemDetailDescription.parent.requestDisallowInterceptTouchEvent(false)
            itemDetailsMapViewTransparent.parent.requestDisallowInterceptTouchEvent(false)
            false
        }

        itemDetailDescription.setOnTouchListener { v, _ ->
            val descriptionView = (v as MaterialTextView)
            v.performClick()
            if (descriptionView.lineCount > descriptionView.maxLines) {
                itemDetailDescription.parent.requestDisallowInterceptTouchEvent(true)
                false
            } else {
                true
            }
        }

        itemDetailsMapViewTransparent.setOnTouchListener { view, event ->
            view.performClick()
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    scrollView.requestDisallowInterceptTouchEvent(true)
                    false
                }
                MotionEvent.ACTION_UP -> {
                    scrollView.requestDisallowInterceptTouchEvent(false)
                    true
                }
                MotionEvent.ACTION_MOVE -> {
                    scrollView.requestDisallowInterceptTouchEvent(true)
                    false
                }
                else -> true
            }
        }
    }

    override fun onFinishEditDialog(buyer: UserProfile?) {
        if (buyer != null)
            userBuyer = buyer
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            BUYER_DIALOG_CODE -> {
                if (resultCode == Activity.RESULT_OK) {

                    editItemButton.isEnabled = false
                    itemReceived.buyerId = userBuyer.id
                    itemReceived.buyerEmail = userBuyer.email
                    itemReceived.status = ItemStatus.WAITING_CONFIRMATION.status
                    itemReceived.editable = false
                    chooseBuyerButton.text = "Waiting for confirmation"
                    chooseBuyerButton.isEnabled = false
                    loadItemStatus()
                    ItemFirebaseRepository().saveItem(itemReceived)

                    viewAdapter = MyInterestedUserProfileAdapter(
                        userInterested,
                        itemReceived,
                        viewLifecycleOwner,
                        object : ContextProvider {
                            override fun getContext(): Context {
                                return activity!!.applicationContext
                            }
                        }
                    )
                    recyclerView =
                        requireView().findViewById<RecyclerView>(R.id.userInterestedRecyclerView)
                            .apply {
                                // use a linear layout manager
                                layoutManager = viewManager
                                // specify an viewAdapter (see also next example)
                                adapter = viewAdapter
                            }
                }
            }
        }

    }

    private fun editItem() {
        val bundle = Bundle()

        bundle.putParcelable("item", itemReceived)
        if (this::userProfileItem.isInitialized)
            bundle.putParcelable("userProfileItem", userProfileItem)
        if (this::itemImage.isInitialized)
            bundle.putBoolean("itemImage", true)

        // Navigate to ItemEditFragment
        findNavController().navigate(R.id.action_itemDetailsFragment_to_itemEditFragment, bundle)
    }

    private fun loadInterestedUser(item: Item) {
        userInterestedRecyclerView.visibility = View.VISIBLE
        userInterestedInfo.visibility = View.VISIBLE

        showUserInterestedProgressBar()
        val interestRepo = ViewModelProvider(this).get(InterestViewModel::class.java)
        val userRepo = ViewModelProvider(this).get(UserFirebaseViewModel::class.java)

        interestRepo.getInterestsForItem(item.id).observe(viewLifecycleOwner, Observer { interest ->

            if (interest.isNullOrEmpty()) {
                userInterestedEmptyList.visibility = View.VISIBLE
                userInterestedRecyclerView.visibility = View.GONE
                chooseBuyerButton.visibility = View.INVISIBLE
                hideUserInterestedProgressbar()
                return@Observer
            }
            userRepo.getSavedUsersByIds(interest.map { it.userId })
                .observe(viewLifecycleOwner, Observer { interestUsers ->
                    userInterested.value = interestUsers
                    users = interestUsers

                    viewManager = LinearLayoutManager(requireContext())
                    viewAdapter = MyInterestedUserProfileAdapter(
                        userInterested,
                        itemReceived,
                        viewLifecycleOwner,
                        object : ContextProvider {
                            override fun getContext(): Context {
                                return activity!!.applicationContext
                            }
                        }
                    )
                    recyclerView =
                        requireView().findViewById<RecyclerView>(R.id.userInterestedRecyclerView)
                            .apply {
                                // use a linear layout manager
                                layoutManager = viewManager
                                // specify an viewAdapter (see also next example)
                                adapter = viewAdapter
                            }

                    if (interestUsers.isNullOrEmpty()) {
                        userInterestedEmptyList.visibility = View.VISIBLE
                        userInterestedRecyclerView.visibility = View.GONE
                        chooseBuyerButton.visibility = View.INVISIBLE
                    } else {
                        userInterestedEmptyList.visibility = View.INVISIBLE
                        userInterestedRecyclerView.visibility = View.VISIBLE
                        chooseBuyerButton.visibility = View.VISIBLE
                    }

                    if (interestUsers.map { user -> user.id }
                            .contains(itemReceived.buyerId) && itemReceived.status == ItemStatus.WAITING_CONFIRMATION.status) {
                        chooseBuyerButton.text = "Waiting for confirmation"
                        chooseBuyerButton.isEnabled = false
                    } else if (interestUsers.map { user -> user.id }
                            .contains(itemReceived.buyerId) && itemReceived.status == ItemStatus.SOLD.status) {
                        chooseBuyerButton.visibility = View.GONE
                    } else if (itemReceived.status == ItemStatus.BLOCKED.status) {
                        chooseBuyerButton.text = "Unblock your item"
                        chooseBuyerButton.isEnabled = false
                    }

                    hideUserInterestedProgressbar()

                })

        })
    }

    private fun showProgressBar() {
        progressBarItemDetails.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBarItemDetails.visibility = View.INVISIBLE
    }

    private fun showUserInterestedProgressBar() {
        progressBarUserInterested.visibility = View.VISIBLE
    }

    private fun hideUserInterestedProgressbar() {
        progressBarUserInterested.visibility = View.INVISIBLE
    }

    private fun hideViewsForLoading() {
        itemDetailPhoto.visibility = View.INVISIBLE
        itemDetailTitle.visibility = View.INVISIBLE
        euro.visibility = View.INVISIBLE
        itemDetailPrice.visibility = View.INVISIBLE
        itemDetailStatus.visibility = View.INVISIBLE
        itemDetailDescription.visibility = View.INVISIBLE
        info.visibility = View.INVISIBLE
        itemDetailCategoryLabel.visibility = View.INVISIBLE
        itemDetailCategory.visibility = View.INVISIBLE
        itemDetailSubcategoryLabel.visibility = View.INVISIBLE
        itemDetailSubcategory.visibility = View.INVISIBLE
        saleInfo.visibility = View.INVISIBLE
        locationInfo.visibility = View.INVISIBLE
        itemDetailLocationLabel.visibility = View.INVISIBLE
        itemDetailLocation.visibility = View.INVISIBLE
        itemDetailLocationLabelState.visibility = View.INVISIBLE
        itemDetailLocationState.visibility = View.INVISIBLE
        itemCreationDateLabel.visibility = View.INVISIBLE
        itemDetailCreationDate.visibility = View.INVISIBLE
        itemDetailExpiryDateLabel.visibility = View.INVISIBLE
        itemDetailExpiryDate.visibility = View.INVISIBLE
        fabInterest.visibility = View.INVISIBLE
        fabNotInterest.visibility = View.INVISIBLE
        chooseBuyerButton.visibility = View.INVISIBLE
        itemDetailsMapViewContainer.visibility = View.GONE
        itemDetailsDirections.visibility = View.GONE

        if (this::editItemButton.isInitialized)
            editItemButton.isEnabled = false

    }

    private fun restoreViewsAfterLoading() {
        itemDetailPhoto.visibility = View.VISIBLE
        itemDetailTitle.visibility = View.VISIBLE
        euro.visibility = View.VISIBLE
        itemDetailPrice.visibility = View.VISIBLE
        itemDetailStatus.visibility = View.VISIBLE
        itemDetailDescription.visibility = View.VISIBLE
        info.visibility = View.VISIBLE
        itemDetailCategoryLabel.visibility = View.VISIBLE
        itemDetailCategory.visibility = View.VISIBLE
        itemDetailSubcategoryLabel.visibility = View.VISIBLE
        itemDetailSubcategory.visibility = View.VISIBLE
        saleInfo.visibility = View.VISIBLE
        locationInfo.visibility = View.VISIBLE
        itemDetailLocationLabel.visibility = View.VISIBLE
        itemDetailLocationLabelState.visibility = View.VISIBLE
        itemDetailLocation.visibility = View.VISIBLE
        itemDetailLocationState.visibility = View.VISIBLE
        itemCreationDateLabel.visibility = View.VISIBLE
        itemDetailCreationDate.visibility = View.VISIBLE
        itemDetailExpiryDateLabel.visibility = View.VISIBLE
        itemDetailExpiryDate.visibility = View.VISIBLE
        itemDetailsMapViewContainer.visibility = View.VISIBLE

        if (itemDetailsStatus != ItemDetailsStatus.FROM_PERSONAL.status && itemDetailsStatus != ItemDetailsStatus.FROM_EDIT.status) {
            itemDetailsDirections.visibility = View.VISIBLE
            itemDetailsDirections.isClickable = true
        }

        if (this::editItemButton.isInitialized && itemReceived.editable)
            editItemButton.isEnabled = true
    }

    /*
        Request location permissions to user
     */
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            42
        )
        // Request code 42 is for the location
    }

}
