package com.mad2020.group09.buybye.enums

enum class ItemStatus(val status: String) {
    DEFAULT("New item"),
    ON_SALE("On sale"),
    SOLD("Sold"),
    BLOCKED("Blocked"),
    WAITING_CONFIRMATION("To confirm")
}