package com.mad2020.group09.buybye.ui.items

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.adapters.BoughtItemsListAdapter
import com.mad2020.group09.buybye.adapters.ContextProvider
import com.mad2020.group09.buybye.model.Item
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.ItemFirebaseViewModel
import com.mad2020.group09.buybye.utilities.getCurrentUser
import kotlinx.android.synthetic.main.fragment_bought_items_list.*

class BoughtItemsListFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: BoughtItemsListAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var searchView: SearchView
    private lateinit var userProfile: UserProfile

    var data = MutableLiveData<List<Item>>()
    lateinit var da: List<Item>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_bought_items_list, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.mylist_menu, menu) // same menu as ItemListFragment
        searchView =  menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setOnCloseListener { false }

        val searchPlate = searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        searchPlate.hint = "Search" // Searching by title, category, subcategory or location
        val searchPlateView: View = searchView.findViewById(androidx.appcompat.R.id.search_plate)
        searchPlateView.setBackgroundColor(
            ContextCompat.getColor(
                requireActivity().applicationContext,
                android.R.color.transparent
            )
        )

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                // method called after the user clicks the enter/search after inputting the search query

                viewAdapter.filter.filter(query)
                if (viewAdapter.itemCount == 0)
                    Toast.makeText(requireContext(), "No match found.", Toast.LENGTH_SHORT).show()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // method used when the change occurs in the query field

                viewAdapter.filter.filter(newText)

                return false
            }
        })
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtEmptyList.visibility = View.INVISIBLE
        getItemsFromDB()
    }

    private fun getItemsFromDB() {
        showProgressBar()
        val itemRepo = ViewModelProvider(this).get(ItemFirebaseViewModel::class.java)
        getCurrentUser().observe(viewLifecycleOwner, Observer { currentUser ->
            userProfile = currentUser!!
            val items = itemRepo.getSavedItemsByBuyer(userProfile.id)

            items.observe(viewLifecycleOwner, Observer {
                data.value = it.sortedBy { item -> item.boughtDate }.reversed()
                da = it!!

                viewManager = LinearLayoutManager(this.context)
                viewAdapter = BoughtItemsListAdapter(
                    data,
                    userProfile,
                    viewLifecycleOwner,
                    object : ContextProvider {
                        override fun getContext(): Context {
                            return activity!!.applicationContext
                        }
                    }
                )
                recyclerView = requireView().findViewById<RecyclerView>(R.id.bought_items_recycler_view).apply {
                    // use a linear layout manager
                    layoutManager = viewManager
                    // specify an viewAdapter (see also next example)
                    adapter = viewAdapter
                }

                if (data.value.isNullOrEmpty()) {
                    txtEmptyList.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                } else {
                    txtEmptyList.visibility = View.INVISIBLE
                    recyclerView.visibility = View.VISIBLE
                }
            })
            hideProgressBar()
        })

    }

    private fun showProgressBar() {
        progressBoughtItemsList.visibility=View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBoughtItemsList.visibility=View.INVISIBLE
    }


}