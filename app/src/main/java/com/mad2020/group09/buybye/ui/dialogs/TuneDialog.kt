package com.mad2020.group09.buybye.ui.dialogs

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.ui.home.OnSaleListFragment
import kotlinx.android.synthetic.main.fragment_tune_dialog.*


class TuneDialog : DialogFragment() {

    private lateinit var inflater: LayoutInflater
    private lateinit var inCat: EditText
    private var maxPrice: Int = 0
    private var minPrice: Int = 0
    private lateinit var tv: TextView
    private var arrayCat = mutableSetOf<Int>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        inflater = requireActivity().layoutInflater
        val builder = AlertDialog.Builder(requireContext())
        val v = inflater.inflate(R.layout.fragment_tune_dialog, null)
        builder.setView(v)
        tv = v.findViewById(R.id.priceRange)
        builder.setPositiveButton("Search"
        ) { dialog, which ->
            val dialogListener = targetFragment!! as DialogListener
            dialogListener.onFinishEditDialog(arrayCat.toMutableList(), minPrice, maxPrice)
            dismiss()
            targetFragment!!.onActivityResult(
                targetRequestCode,
                Activity.RESULT_OK,
                requireActivity().getIntent()
            )
        }
        builder.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                dismiss()
            }
        })

        v.findViewById<CheckBox>(R.id.checkBox_cat1).setOnClickListener {
            onCheckboxClicked(it, 1)
        }
        v.findViewById<CheckBox>(R.id.checkBox_cat2).setOnClickListener {
            onCheckboxClicked(it, 2)
        }
        v.findViewById<CheckBox>(R.id.checkBox_cat3).setOnClickListener {
            onCheckboxClicked(it, 3)
        }
        v.findViewById<CheckBox>(R.id.checkBox_cat4).setOnClickListener {
            onCheckboxClicked(it, 4)
        }
        v.findViewById<CheckBox>(R.id.checkBox_cat5).setOnClickListener {
            onCheckboxClicked(it, 5)
        }
        v.findViewById<CheckBox>(R.id.checkBox_cat6).setOnClickListener {
            onCheckboxClicked(it, 6)
        }
        v.findViewById<CheckBox>(R.id.checkBox_cat7).setOnClickListener {
            onCheckboxClicked(it, 7)
        }
        v.findViewById<CheckBox>(R.id.checkBox_cat8).setOnClickListener {
            onCheckboxClicked(it, 8)
        }

        val seekBar = v.findViewById(R.id.seekBar) as SeekBar
        seekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) {
                maxPrice = progress * 25
                if (maxPrice != 0) {
                    tv.text = minPrice.toString() + " - " + maxPrice.toString() + " €"
                    //Toast.makeText(requireContext(), maxPrice.toString() + "", Toast.LENGTH_LONG).show()
                } else {
                    tv.text = "All prices"
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        if (arguments != null) {
            var categories = arguments?.getIntegerArrayList("categories")
            if (categories != null) {
                for (cat in categories) {
                    when (cat) {
                        1 -> v.findViewById<CheckBox>(R.id.checkBox_cat1).isChecked = true
                        2 -> v.findViewById<CheckBox>(R.id.checkBox_cat2).isChecked = true
                        3 -> v.findViewById<CheckBox>(R.id.checkBox_cat3).isChecked = true
                        4 -> v.findViewById<CheckBox>(R.id.checkBox_cat4).isChecked = true
                        5 -> v.findViewById<CheckBox>(R.id.checkBox_cat5).isChecked = true
                        6 -> v.findViewById<CheckBox>(R.id.checkBox_cat6).isChecked = true
                        7 -> v.findViewById<CheckBox>(R.id.checkBox_cat7).isChecked = true
                        8 -> v.findViewById<CheckBox>(R.id.checkBox_cat8).isChecked = true
                    }
                    arrayCat.add(cat)
                }
            }
            var price = arguments?.getInt("price")
            if (price != null) {
                seekBar.progress = price / 25
            }
        }

        return builder.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var setFullScreen = false
        if (arguments != null) {
            setFullScreen = requireNotNull(arguments?.getBoolean("fullScreen"))
        }
        if (setFullScreen)
            setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
    }

    interface DialogListener {
        fun onFinishEditDialog(inputCat: MutableList<Int>, minPrice: Int, maxPrice: Int)
    }

    fun onCheckboxClicked(view: View, count: Int) {
        if (view is CheckBox) {
            val checked: Boolean = view.isChecked

            if (checked) {
                // Put in list
                arrayCat.add(count)
            } else {
                // Remove
                arrayCat.remove(count)
            }
        }
    }
}