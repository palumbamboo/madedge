package com.mad2020.group09.buybye.model

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.Gson
import com.mad2020.group09.buybye.enums.ItemStatus
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*

@Keep
@Parcelize
data class Item(
    var title: String = "Item Title",
    var description: String = "Item Description",
    var price: Double = 0.0,
    var category: String = "Category not specified",
    var subcategory: String = "Subcategory not specified",
    var location: String = "Location not specified",
    var country: String = "Country not specified",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var status: String = ItemStatus.DEFAULT.status,
    var expiryDate: Date = Date(),
    var sellerId: String = "",
    var sellerEmail: String = "",
    var buyerId: String? = null,
    var buyerEmail: String? = null
) : Parcelable {

    @IgnoredOnParcel
    var id: String = ""

    init {
        if (this.id == "") {
            id = UUID.randomUUID().toString()
                .replace("-", "")
                .toUpperCase(Locale.getDefault())
        }
    }

    @IgnoredOnParcel
    var photoId: String = "item_photo_$id"

    @IgnoredOnParcel
    var creationDate: Date = Date()

    @IgnoredOnParcel
    var editable: Boolean = true

    @IgnoredOnParcel
    var boughtDate: Date? = null

    fun exportJson(): String? {
        return Gson().toJson(this)
    }

    override fun describeContents(): Int {
        return 0
    }

    fun updateId(newId: String) {
        photoId = "item_photo_$newId"
        this.id = newId
    }

    fun expired(): Boolean {
        return Date() <= expiryDate
    }

    fun validForSale(): Boolean {
        return expired() && status == ItemStatus.ON_SALE.status
    }
}