package com.mad2020.group09.buybye.ui.login

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.ImageRepository
import com.mad2020.group09.buybye.repositories.UserFirebaseViewModel
import com.mad2020.group09.buybye.utilities.cropAndResizeBitmap
import com.mad2020.group09.buybye.utilities.fileExist
import com.mad2020.group09.buybye.utilities.getCurrentUser
import com.mad2020.group09.buybye.utilities.readBitmapFromStorageMemory
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment: Fragment() {

    companion object {
        private const val TAG = "LOGIN_FRAGMENT"
    }

    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var auth: FirebaseAuth
    private lateinit var navView: NavigationView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navView = requireActivity().findViewById(R.id.nav_view)

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            //.requestIdToken("647688483088-fod94oh4jphmrpn5shdntei56j9coe9t.apps.googleusercontent.com")
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)

        auth = Firebase.auth

        val tv = signInButton.getChildAt(0) as TextView
        tv.text = "Sign in"

        signInButton.setOnClickListener{
            signInButtonClicked()
        }

        signOutButton.setOnClickListener {
            signOutButtonClicked()
        }

    }

    override fun onStart() {
        super.onStart()
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        //val account = GoogleSignIn.getLastSignedInAccount(requireContext())
        // replace signIn account with firebase account
        // updateUI(account)
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(user: FirebaseUser?) {
        hideProgressBar()
        // hide the sign in button, launch your main activity or whatever is appropriate for your app
        // different operations if account is null or not null
        // If GoogleSignIn.getLastSignedInAccount returns a GoogleSignInAccount object
        // (rather than null), the user has already signed in to your app with Google.
        // Update your UI accordingly—that is, hide the sign-in button, launch your main
        // activity, or whatever is appropriate for your app.
        val headerView = navView.getHeaderView(0)
        if (user != null) {
            // If GoogleSignIn.getLastSignedInAccount returns null, the user has not yet signed
            // in to your app with Google. Update your UI to display the Google Sign-in button.
            navView.menu.findItem(R.id.nav_myList).setVisible(true)
            navView.menu.findItem(R.id.nav_profile).setVisible(true)
            navView.menu.findItem(R.id.nav_itemsOfInterest).setVisible(true)
            navView.menu.findItem(R.id.nav_boughtItemsList).setVisible(true)
            navView.menu.findItem(R.id.nav_itemsToConfirm).setVisible(true)
            navView.menu.findItem(R.id.nav_login).title = "Logout"
            navView.menu.findItem(R.id.nav_login).setIcon(R.drawable.logout)


            if (Firebase.auth.currentUser != null) {
                navView.menu.findItem(R.id.nav_login).title = "Logout"

                val userRepo = ViewModelProvider(this).get(UserFirebaseViewModel::class.java)
                val currentUserDb = Firebase.auth.currentUser!!.email?.let {
                    UserFirebaseViewModel().getSavedUserByEmail(
                        it
                    )
                }
                if (currentUserDb != null) {
                    currentUserDb.observe(this, Observer { currentUser: UserProfile ->
                        if (currentUser.nickname != "") {
                            headerView.findViewById<TextView>(R.id.headerTitle).text =
                                currentUser.nickname
                        }
                        headerView.findViewById<TextView>(R.id.headerDescription).text = currentUser.email

                        if (fileExist(currentUser.photoId, requireContext(), true)) {
                            headerView.findViewById<ImageView>(R.id.headerImage).setImageBitmap(
                                readBitmapFromStorageMemory(currentUser.photoId, requireContext())?.let {
                                    cropAndResizeBitmap(
                                        it, 208)
                                })
                        }
                        else {
                            val imageRepo = ViewModelProvider(this).get(ImageRepository::class.java)
                            val image = imageRepo.getUserImage(currentUser.photoId)

                            image.observe(this, Observer { userImage: Bitmap? ->

                                if (userImage != null) {
                                    userImage.let {
                                        headerView.findViewById<ImageView>(R.id.headerImage)
                                            .setImageBitmap(userImage?.let { it1 ->
                                                cropAndResizeBitmap(
                                                    it1,
                                                    208
                                                )
                                            })

                                    }
                                }
                                else {
                                    headerView.findViewById<ImageView>(R.id.headerImage).setImageBitmap(
                                        cropAndResizeBitmap(ContextCompat.getDrawable(requireActivity(), R.drawable.my_avatar)!!.toBitmap(), 70))
                                }
                            })
                        }
                    })
                }
            }


            signOutButton.visibility = View.VISIBLE
            googleSignIn.text = "Signed in with your Google account"
            signInButton.visibility = View.INVISIBLE
            authProfile.text = user.displayName
            authEmail.text = user.email
            authProfile.visibility = View.VISIBLE
            authEmail.visibility = View.VISIBLE
        }
        else {
            navView.menu.findItem(R.id.nav_myList).setVisible(false)
            navView.menu.findItem(R.id.nav_profile).setVisible(false)
            navView.menu.findItem(R.id.nav_itemsOfInterest).setVisible(false)
            navView.menu.findItem(R.id.nav_boughtItemsList).setVisible(false)
            navView.menu.findItem(R.id.nav_itemsToConfirm).setVisible(false)
            navView.menu.findItem(R.id.nav_login).title = "Login"
            navView.menu.findItem(R.id.nav_login).setIcon(R.drawable.login)
            headerView.findViewById<TextView>(R.id.headerTitle).text = "BuyBye"
            headerView.findViewById<TextView>(R.id.headerDescription).text = "Buy first, Bye next!"
            headerView.findViewById<ImageView>(R.id.headerImage).setImageResource(R.mipmap.ic_launcher_round)
            googleSignIn.text = "Sign in with your Google account"
            signOutButton.visibility = View.INVISIBLE
            signInButton.visibility = View.VISIBLE
            authProfile.visibility = View.INVISIBLE
            authEmail.visibility = View.INVISIBLE
        }
    }

    private fun signInButtonClicked() {
        val signInIntent: Intent = mGoogleSignInClient.getSignInIntent()
        startActivityForResult(signInIntent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == 1) {
            // The Task returned from this call is always completed, no need to attach a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            // Google Sign In was successful, authenticate with Firebase
            Log.d("LOGIN-USER", "signInResult:successful")
            if (account != null) {
                Log.d("LOGIN-FIREBASE", "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
                createNewAccountOnFirebase(account.email!!, account.displayName!!)
            }
            //updateUI(account)
            //Toast.makeText(requireContext(), "Login successful.", Toast.LENGTH_SHORT).show()
        } catch (e: ApiException) { // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("LOGIN-USER", "signInResult:failed code=" + e.statusCode)
            //Toast.makeText(requireContext(), "Login failed. Please try again.", Toast.LENGTH_SHORT ).show()
            updateUI(null)
        }
    }

    private fun signOutButtonClicked() {
        // Firebase sign out
        auth.signOut()

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener { updateInfoView(false) }
    }

    private fun disconnectButtonClicked() {
        // Firebase sign out
        auth.signOut()

        // Google revoke access
        mGoogleSignInClient.revokeAccess().addOnCompleteListener { updateInfoView(false) }
    }

    private fun updateInfoView(signingIn: Boolean) {
        hideProgressBar()
        if (signingIn == false) { // I am signing out
            navView.menu.findItem(R.id.nav_myList).setVisible(false)
            navView.menu.findItem(R.id.nav_profile).setVisible(false)
            navView.menu.findItem(R.id.nav_itemsOfInterest).setVisible(false)
            navView.menu.findItem(R.id.nav_boughtItemsList).setVisible(false)
            navView.menu.findItem(R.id.nav_itemsToConfirm).setVisible(false)

            navView.menu.findItem(R.id.nav_login).title = "Login"
            navView.menu.findItem(R.id.nav_login).setIcon(R.drawable.login)

            val headerView = navView.getHeaderView(0)
            headerView.findViewById<TextView>(R.id.headerTitle).text = "BuyBye"
            headerView.findViewById<TextView>(R.id.headerDescription).text =
                "Buy first, Bye next!"
            headerView.findViewById<ImageView>(R.id.headerImage).setImageResource(R.mipmap.ic_launcher_round)
            signOutButton.visibility = View.INVISIBLE
            signInButton.visibility = View.VISIBLE
            googleSignIn.text = "Sign in with your Google account"
            authProfile.visibility = View.INVISIBLE
            authEmail.visibility = View.INVISIBLE
            Toast.makeText(requireContext(),"Logout successful.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        showProgressBar()
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("LOGIN-FIREBASE", "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)
                    getCurrentUser().observe(viewLifecycleOwner, Observer { currentUser ->
                        val userProfile = currentUser!!
                        FirebaseMessaging.getInstance().subscribeToTopic(userProfile.id)
                        .addOnCompleteListener { task ->
                            var msg = "Subscribed to personal topic!"
                            if (!task.isSuccessful) {
                                msg = "Error topic"
                            }
                            Log.d("subscribe", msg + " id: " + userProfile.id)
                        }
                    })
                    Toast.makeText(requireContext(), "Login successful.", Toast.LENGTH_SHORT).show()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("LOGIN-FIREBASE", "signInWithCredential:failure", task.exception)
                    updateUI(null)
                    Toast.makeText(requireContext(), "Login failed. Please try again.", Toast.LENGTH_SHORT ).show()
                }
                hideProgressBar()
            }
    }

    private fun createNewAccountOnFirebase(email: String, name: String) {
        val userRepo = UserFirebaseViewModel()
        val userDb = userRepo.getSavedUserByEmail(email)

        userDb.observe(this, Observer {
            if (it != null) {
                Log.d(TAG, "User $email exists!")
            } else {
                val newUserProfile = UserProfile()
                newUserProfile.email = email
                newUserProfile.name  = name
                UserFirebaseViewModel().saveUserToFirebase(newUserProfile)
                Log.d(TAG, "New user $email added to database")
            }
        })
    }

    private fun showProgressBar() {
        progressBar.visibility=View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility=View.INVISIBLE
    }

}

