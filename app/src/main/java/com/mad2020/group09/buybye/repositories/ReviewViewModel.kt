package com.mad2020.group09.buybye.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.EventListener
import com.mad2020.group09.buybye.model.Review
import com.mad2020.group09.buybye.model.UserProfile

class ReviewViewModel: ViewModel() {
    companion object {
        private const val TAG = "REVIEW_FIREBASE_VIEW_MODEL"
    }

    private val reviewRepository = ReviewRepository()

    // Save Review
    fun saveReviewToFirebase(review: Review){
        reviewRepository.saveReview(review).addOnFailureListener{
            Log.e(TAG, "SAVE FAILED on Review: ${it.message}")
        }.addOnSuccessListener {
            Log.i(TAG, "SAVE DONE on Review: Review ${review.id}")
        }
    }

    // Get all reviews related to an item
    fun getReviewsByUserId(userId: String): LiveData<List<Review>> {
        val savedReviewsByUser: MutableLiveData<List<Review>> = MutableLiveData()
        reviewRepository.getReviewsByUserId(userId).addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Reviews: ${e.message}", e)
                savedReviewsByUser.value = null
                return@EventListener
            }

            Log.d(TAG, "LISTEN DONE on Reviews by user: loaded ${value!!.size()}")

            val savedItemList: MutableList<Review> = mutableListOf()
            for(doc in value) {
                val item = doc.toObject(Review::class.java)
                savedItemList.add(item)
            }
            savedReviewsByUser.value = savedItemList
        })
        return savedReviewsByUser
    }

    fun getReviewByUserAndItem(userId: String, itemId: String): LiveData<Review> {
        val savedReviewByUserAndItem: MutableLiveData<Review> = MutableLiveData()
        reviewRepository.getReviewByUserAndItem(userId, itemId).addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Reviews: ${e.message}", e)
                savedReviewByUserAndItem.value = null
                return@EventListener
            }

            Log.d(TAG, "LISTEN DONE on Reviews by user and item: loaded ${value!!.size()} review")
            if(value.size() < 1) {
                savedReviewByUserAndItem.value = null
            } else {
                savedReviewByUserAndItem.value = value.first().toObject(Review::class.java)
            }
        })
        return savedReviewByUserAndItem
    }

    // Delete Review
    fun deleteReview(review: Review){
        reviewRepository.deleteReview(review).addOnFailureListener{
            Log.e(TAG, "DELETE FAILED on Review: ${it.message}")
        }
    }
}