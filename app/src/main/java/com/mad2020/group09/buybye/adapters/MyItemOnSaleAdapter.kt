package com.mad2020.group09.buybye.adapters

import android.content.res.ColorStateList
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.google.android.material.card.MaterialCardView
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.enums.ItemDetailsStatus
import com.mad2020.group09.buybye.enums.ItemStatus
import com.mad2020.group09.buybye.model.Item
import com.mad2020.group09.buybye.repositories.ImageRepository
import com.mad2020.group09.buybye.ui.home.OnSaleListFragment
import com.mad2020.group09.buybye.utilities.*
import kotlinx.android.synthetic.main.fragment_item_card.view.*
import kotlinx.android.synthetic.main.fragment_item_details.*
import java.util.*


class MyItemOnSaleAdapter(
    private val myItemDataset: LiveData<List<Item>>,
    private val lifecycleOwner: LifecycleOwner,
    private val myContextProvider: ContextProvider
) : LifecycleRecyclerAdapter<MyItemOnSaleAdapter.MyViewHolder>(), Filterable {

    var filterItemDataset: MutableLiveData<List<Item>> =
        MutableLiveData(myItemDataset.value) as MutableLiveData<List<Item>>

    private var availableSize = screenAvailableSize(myContextProvider.getContext())


    class MyViewHolder(private val cardView: CardView) : LifecycleViewHolder(cardView) {

        fun bindItems(
            item: Item,
            availableSize: Int,
            lifecycleOwner: LifecycleOwner,
            contextProvider: ContextProvider
        ) {

            val realAvailableSize = availableSize - cardView.marginStart - cardView.marginEnd

            showProgressBar()
            cardView.itemImage.visibility = View.INVISIBLE

            cardView.itemTitle.text = item.title
            cardView.itemPrice.text = "${item.price} €"
            cardView.itemDescription.text = item.description
            cardView.itemLocation.text = item.location

            loadItemStatus(item.status, contextProvider)

            val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.ITALY)
            cardView.itemCreationDate.text = formatter.format(item.creationDate)

            if (fileExist(
                    item.photoId,
                    contextProvider.getContext(),
                    true
                )
            ) {
                hideProgressBar()
                cardView.itemImage.setImageBitmap(
                    cropAndResizeBitmap(
                        readBitmapFromStorageMemory(item.photoId, contextProvider.getContext())!!,
                        realAvailableSize
                    )
                )
                cardView.itemEditButton.isEnabled = true
                cardView.itemImage.visibility = View.VISIBLE
            } else {
                ImageRepository().getItemImage(item.photoId).observe(lifecycleOwner, Observer {
                    hideProgressBar()
                    if (it != null) {
                        cardView.itemImage.setImageBitmap(
                            cropAndResizeBitmap(
                                it,
                                realAvailableSize
                            )
                        )
                        writeBitmapToStorageMemory(
                            it, openFile(
                                item.photoId,
                                contextProvider.getContext()
                            )
                        )
                    } else {
                        cardView.itemImage.setImageResource(R.drawable.item_label)
                    }
                    cardView.itemEditButton.isEnabled = true
                    cardView.itemImage.visibility = View.VISIBLE
                })
            }
            cardView.setOnClickListener {
                it.findNavController().navigate(
                    R.id.itemDetailsFragment,
                    bundleOf(
                        "item" to item,
                        "itemDetailStatus" to ItemDetailsStatus.FROM_ON_SALE.status
                    )
                )
            }
            cardView.itemEditButton.text = "Seller Profile"
            cardView.itemEditButton.setOnClickListener {
                it.findNavController().navigate(
                    R.id.showProfileFragment,
                    bundleOf("item" to item, "fromOnSale" to true)
                )
            }
        }

        private fun showProgressBar() {
            cardView.progressBarCardImage.visibility = View.VISIBLE
        }

        private fun hideProgressBar() {
            cardView.progressBarCardImage.visibility = View.INVISIBLE
        }

        private fun loadItemStatus(status: String, contextProvider: ContextProvider) {
            cardView.itemDetailStatus.text = status
            when (status) {
                ItemStatus.BLOCKED.status -> {
                    cardView.itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            contextProvider.getContext(),
                            R.color.blocked_chip
                        )
                    )
                    cardView.itemDetailStatus.setTextColor(Color.BLACK)
                }
                ItemStatus.ON_SALE.status -> {
                    cardView.itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            contextProvider.getContext(),
                            R.color.onsale_chip
                        )
                    )
                    cardView.itemDetailStatus.setTextColor(Color.BLACK)
                }
                ItemStatus.SOLD.status -> {
                    cardView.itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            contextProvider.getContext(),
                            R.color.sold_chip
                        )
                    )
                    cardView.itemDetailStatus.setTextColor(Color.WHITE)
                }
                ItemStatus.WAITING_CONFIRMATION.status -> {
                    cardView.itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            contextProvider.getContext(),
                            R.color.toconfirm_chip
                        )
                    )
                    cardView.itemDetailStatus.setTextColor(Color.BLACK)
                }
            }
        }


    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        // create a new view
        val card = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_item_card, parent, false) as MaterialCardView

        return MyViewHolder(
            card
        )
    }

    override fun getItemCount(): Int {
        return filterItemDataset.value!!.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item: Item = filterItemDataset.value!![position]

        holder.bindItems(item, availableSize, lifecycleOwner, myContextProvider)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                if (constraint.isNullOrBlank()) {
                    val filterResults = FilterResults()
                    filterResults.values = myItemDataset.value!!.filter { true }
                    return filterResults
                }
                val constraintArray = constraint.toString().split("_")
                var filterTag: String = constraintArray[0]

                if (filterTag != null && filterTag == OnSaleListFragment.ON_SALE_TAG) {

                    val filterResults = FilterResults()
                    val remainingFromSplit = constraintArray[1]
                    if (remainingFromSplit.isNullOrBlank()) {
                        filterResults.values = myItemDataset.value!!.filter { true }
                    } else {
                        filterResults.values = myItemDataset.value!!.filter {
                            it.title.contains(remainingFromSplit, ignoreCase = true) ||
                                    it.category.contains(remainingFromSplit, ignoreCase = true) ||
                                    it.subcategory.contains(
                                        remainingFromSplit,
                                        ignoreCase = true
                                    ) ||
                                    it.location.contains(remainingFromSplit, ignoreCase = true)
                        }
                    }
                    return filterResults
                } else if (filterTag != null && filterTag == OnSaleListFragment.TUNE_TAG) {
                    var filterMaxPrice: Int? = constraintArray[1].toInt()
                    var filterCategories: List<String?>? = if (constraintArray.size == 3)
                        constraintArray[2].split("|")
                    else
                        listOf()

                    var filterResults = FilterResults()
                    if (filterCategories.isNullOrEmpty()) {
                        if (filterMaxPrice == null || filterMaxPrice == 0) {
                            filterResults.values = myItemDataset.value!!.filter { true }
                        } else {
                            filterResults.values = myItemDataset.value!!.filter {
                                it.price <= filterMaxPrice
                            }
                        }
                    } else {
                        if (filterMaxPrice == null || filterMaxPrice == 0) {
                            filterResults.values = myItemDataset.value!!.filter {
                                filterCategories.contains(it.category)
                            }
                        } else {
                            filterResults.values = myItemDataset.value!!.filter {
                                it.price <= filterMaxPrice && filterCategories.contains(it.category)
                            }
                        }

                    }

                    return filterResults
                } else {
                    // Filter tag not present
                    // Do not filter
                    val filterResults = FilterResults()
                    filterResults.values = myItemDataset.value!!.filter { true }
                    return filterResults
                }

            }

            override fun publishResults(constraint: CharSequence?, filterResults: FilterResults) {
                filterItemDataset.value = filterResults.values as List<Item>
                notifyDataSetChanged()
            }
        }
    }

}


