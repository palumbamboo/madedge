package com.mad2020.group09.buybye.repositories

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.google.firebase.storage.ktx.storageMetadata
import java.io.ByteArrayOutputStream

class ImageRepository : ViewModel() {

    companion object {
        private const val TAG          = "IMAGE_FIREBASE_REPOSITORY"
        private const val IMAGE_PATH   = "images"
        private const val ITEM_FOLDER  = "itemPhotos"
        private const val USER_FOLDER  = "userPhotos"
        private const val TEN_MB: Long = 1024 * 1024 * 10
    }

    private val storage = Firebase.storage
    private val storageRef = storage.reference

    fun uploadUserImage(name: String, image: Bitmap): LiveData<Uri> {
        return uploadImage(name, USER_FOLDER, image)
    }

    fun uploadItemImage(name: String, image: Bitmap): LiveData<Uri> {
        return uploadImage(name, ITEM_FOLDER, image)
    }

    fun getUserImage(name: String): LiveData<Bitmap> {
        return getImage(name, USER_FOLDER)
    }

    fun getItemImage(name: String): LiveData<Bitmap> {
        return getImage(name, ITEM_FOLDER)
    }

    fun deleteUserImage(name: String) {
        deleteImage(name, USER_FOLDER)
    }

    fun deleteItemImage(name: String) {
        deleteImage(name, ITEM_FOLDER)
    }

    private fun uploadImage(name: String, type: String, image: Bitmap): LiveData<Uri> {
        val imageRef   = storageRef.child("$IMAGE_PATH/$type/$name")

        val metadata = storageMetadata {
            contentType = "image/jpeg"
        }

        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val data = stream.toByteArray()
        val uploadTask = imageRef.putBytes(data, metadata)
        val imageUri: MutableLiveData<Uri> = MutableLiveData()

        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    Log.e(TAG, "IMAGE UPLOAD FAILED: ${it.message}")
                }
            }
            imageRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.i(TAG, "IMAGE UPLOAD DONE: ${task.result!!}")
                imageUri.value = task.result!!
            } else {
                imageUri.value  = null
            }
        }
        return imageUri
    }

    private fun getImage(name: String, type: String): LiveData<Bitmap> {
        val imageRef = storageRef.child("$IMAGE_PATH/$type/$name")
        val download: MutableLiveData<Bitmap> = MutableLiveData()
        imageRef.getBytes(TEN_MB).addOnFailureListener{
            Log.e(TAG, "IMAGE DOWNLOAD FAILED: ${it.message}")
            download.value = null
        }.addOnSuccessListener {
            if(it != null) {
                download.value = BitmapFactory.decodeByteArray(it, 0, it.size)
                Log.i(TAG, "IMAGE DOWNLOAD DONE: tot ${it.size} byte")
            } else {
                download.value = null
                Log.w(TAG, "IMAGE DOWNLOAD DONE: image $name not available")
            }
        }
        return download
    }

    private fun deleteImage(name: String, type: String) {
        val imageRef = storageRef.child("$IMAGE_PATH/$type/$name")
        imageRef.delete().addOnSuccessListener {
            Log.i(TAG, "IMAGE DELETION DONE: image $name")
        }.addOnFailureListener {
            Log.e(TAG, "IMAGE DELETION FAILED: ${it.message}")
        }
    }
}