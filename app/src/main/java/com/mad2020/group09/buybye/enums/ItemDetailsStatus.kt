package com.mad2020.group09.buybye.enums

enum class ItemDetailsStatus(val status: String) {
    FROM_ON_SALE("fromOnSale"),
    FROM_INTEREST("fromInterest"),
    FROM_PERSONAL("fromMyItems"),
    FROM_BOUGHT("fromBought"),
    FROM_BOUGHT_CONFIRMATION("fromBoughtConfirmation"),
    FROM_EDIT("fromEdit")
}