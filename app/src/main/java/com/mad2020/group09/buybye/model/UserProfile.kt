package com.mad2020.group09.buybye.model

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.Gson
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 *
 * User class initial implementation
 *
 * Access to general data as name, mail ecc
 *
 */
@Keep
@Parcelize
data class UserProfile(
    var name: String = "",
    var nickname: String = "",
    var email: String = "",
    var location: String = "",
    var country: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var prefix: String = "",
    var phone: String = ""
) : Parcelable {

    @IgnoredOnParcel
    var id: String = ""

    @IgnoredOnParcel
    var overallRating: Double = 0.0

    @IgnoredOnParcel
    var nReviews: Int = 0

    init {
        if (this.id == "") {
            id = UUID.randomUUID().toString()
                .replace("-", "")
                .toUpperCase(java.util.Locale.getDefault())
        }
    }

    @IgnoredOnParcel
    var photoId: String = "user_photo_$id"

    fun exportJson(): String? {
        return Gson().toJson(this)
    }

    fun updateId(newId: String) {
        photoId = "user_photo_$newId"
        this.id = newId
    }

    fun addNewRating(newRating: Double) {
        val previousRating = overallRating * nReviews
        nReviews += 1
        overallRating = (previousRating + newRating) / nReviews
    }

}