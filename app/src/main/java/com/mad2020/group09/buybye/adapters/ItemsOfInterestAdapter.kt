package com.mad2020.group09.buybye.adapters

import android.content.res.ColorStateList
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.google.android.material.card.MaterialCardView
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.enums.ItemDetailsStatus
import com.mad2020.group09.buybye.enums.ItemStatus
import com.mad2020.group09.buybye.model.Item
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.ImageRepository
import com.mad2020.group09.buybye.utilities.*
import kotlinx.android.synthetic.main.fragment_item_card.view.*
import java.util.*

class ItemsOfInterestAdapter(
    private val myItemDataset: LiveData<List<Item>>,
    private val userProfile: UserProfile,
    private val lifecycleOwner: LifecycleOwner,
    private val myContextProvider: ContextProvider
) :
    LifecycleRecyclerAdapter<ItemsOfInterestAdapter.MyViewHolder>(), Filterable {

    var filterItemDataset: MutableLiveData<List<Item>> =
        MutableLiveData(myItemDataset.value) as MutableLiveData<List<Item>>
    private var availableSize = screenAvailableSize(myContextProvider.getContext())


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(private val cardView: CardView) : LifecycleViewHolder(cardView) {

        fun bindItem(
            item: Item,
            userProfile: UserProfile,
            availableSize: Int,
            lifecycleOwner: LifecycleOwner,
            contextProvider: ContextProvider
        ) {

            val realAvailableSize = availableSize - cardView.marginStart - cardView.marginEnd

            showProgressBar()
            cardView.itemImage.visibility = View.INVISIBLE

            cardView.itemTitle.text = item.title
            cardView.itemPrice.text = "${item.price} €"
            cardView.itemDescription.text = item.description
            cardView.itemLocation.text = item.location

            val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.ITALY)
            cardView.itemCreationDate.text = formatter.format(item.creationDate)

            loadItemStatus(item.status, contextProvider)

            var imageIsPresent = false

            if (fileExist(
                    item.photoId,
                    contextProvider.getContext(),
                    true
                )
            ) {
                hideProgressBar()
                cardView.itemImage.setImageBitmap(
                    cropAndResizeBitmap(
                        readBitmapFromStorageMemory(
                            item.photoId,
                            contextProvider.getContext()
                        )!!,
                        realAvailableSize
                    )
                )
                imageIsPresent = true
                cardView.itemEditButton.isEnabled = true
                cardView.itemImage.visibility = View.VISIBLE
            } else {
                ImageRepository().getItemImage(item.photoId).observe(lifecycleOwner, Observer {
                    hideProgressBar()
                    if (it != null) {
                        cardView.itemImage.setImageBitmap(
                            cropAndResizeBitmap(
                                it,
                                realAvailableSize
                            )
                        )
                        writeBitmapToStorageMemory(
                            it, openFile(
                                item.photoId,
                                contextProvider.getContext()
                            )
                        )
                        imageIsPresent = true
                    } else {
                        cardView.itemImage.setImageResource(R.drawable.item_label)
                    }
                    cardView.itemEditButton.isEnabled = true
                    cardView.itemImage.visibility = View.VISIBLE
                })
            }

            cardView.setOnClickListener {
                it.findNavController().navigate(
                    R.id.itemDetailsFragment,
                    bundleOf(
                        "item" to item,
                        "itemDetailStatus" to ItemDetailsStatus.FROM_INTEREST.status,
                        "itemImage" to imageIsPresent,
                        "interestedUser" to userProfile
                    )
                )
            }

            cardView.itemEditButton.text = "Seller Profile"
            cardView.itemEditButton.setOnClickListener {
                it.findNavController().navigate(
                    R.id.showProfileFragment,
                    bundleOf("item" to item, "fromOnSale" to true)
                )
            }

        }

        private fun showProgressBar() {
            cardView.progressBarCardImage.visibility = View.VISIBLE
        }

        private fun hideProgressBar() {
            cardView.progressBarCardImage.visibility = View.INVISIBLE
        }

        private fun loadItemStatus(status: String, contextProvider: ContextProvider) {
            cardView.itemDetailStatus.text = status
            when (status) {
                ItemStatus.BLOCKED.status -> {
                    cardView.itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            contextProvider.getContext(),
                            R.color.blocked_chip
                        )
                    )
                    cardView.itemDetailStatus.setTextColor(Color.BLACK)
                }
                ItemStatus.ON_SALE.status -> {
                    cardView.itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            contextProvider.getContext(),
                            R.color.onsale_chip
                        )
                    )
                    cardView.itemDetailStatus.setTextColor(Color.BLACK)
                }
                ItemStatus.SOLD.status -> {
                    cardView.itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            contextProvider.getContext(),
                            R.color.sold_chip
                        )
                    )
                    cardView.itemDetailStatus.setTextColor(Color.WHITE)
                }
                ItemStatus.WAITING_CONFIRMATION.status -> {
                    cardView.itemDetailStatus.chipBackgroundColor = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            contextProvider.getContext(),
                            R.color.toconfirm_chip
                        )
                    )
                    cardView.itemDetailStatus.setTextColor(Color.BLACK)
                }
            }
        }

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        // create a new view
        val card = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_item_card, parent, false) as MaterialCardView

        return MyViewHolder(
            card
        )
    }

    override fun getItemCount(): Int {
        return filterItemDataset.value!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item: Item = filterItemDataset.value!![position]

        holder.bindItem(item, userProfile, availableSize, lifecycleOwner, myContextProvider)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            //private val filterResults = FilterResults()
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (constraint.isNullOrBlank()) {
                    filterResults.values = myItemDataset.value!!.filter { true }
                } else {
                    filterResults.values = myItemDataset.value!!.filter {
                        it.title.contains(constraint, ignoreCase = true) ||
                                it.category.contains(constraint, ignoreCase = true) ||
                                it.subcategory.contains(constraint, ignoreCase = true) ||
                                it.location.contains(constraint, ignoreCase = true)
                    }
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, filterResults: FilterResults) {
                filterItemDataset.value = filterResults.values as List<Item>
                notifyDataSetChanged()
            }
        }
    }

}