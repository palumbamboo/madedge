package com.mad2020.group09.buybye

import android.annotation.SuppressLint
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.os.Looper
import android.widget.Toast


class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val TAG = "FirebaseMessagingService"

    @SuppressLint("LongLogTag")
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "Dikirim dari: ${remoteMessage.from}")

        if (remoteMessage.notification != null) {
            showNotification(remoteMessage.notification?.body)
        }
    }

    private fun showNotification(body: String?) {
        Looper.prepare() // to be able to make toast
        Toast.makeText(applicationContext, body, Toast.LENGTH_SHORT).show()
        Looper.loop()
    }
}