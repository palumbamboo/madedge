package com.mad2020.group09.buybye.utilities

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.util.Log
import android.util.TypedValue
import java.io.*
import java.util.*

fun writeBitmapToStorageMemory(bitmap: Bitmap, imageFile: File) {
    try {
        // Get the file output stream
        val stream: OutputStream = FileOutputStream(imageFile)
        // Compress bitmap
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        // Flush the stream
        stream.flush()
        // Close stream
        stream.close()
    } catch (e: IOException) { // Catch the exception
        e.printStackTrace()
    }
}

fun readBitmapFromStorageMemory(name: String, context: Context): Bitmap? {
    val wrapper = ContextWrapper(context)
    var file = wrapper.getDir("images", Context.MODE_PRIVATE)
    file = File(file, "$name.jpeg")
    return if(file.exists())
        BitmapFactory.decodeFile(file.toString())
    else
        null
}

fun openFile(name: String, activity: Activity): File {
    val wrapper = ContextWrapper(activity.applicationContext)
    var file    = wrapper.getDir("images", Context.MODE_PRIVATE)
    file        = File(file, "$name.jpeg")
    return file
}

fun openFile(name: String, context: Context): File {
    val wrapper = ContextWrapper(context)
    var file    = wrapper.getDir("images", Context.MODE_PRIVATE)
    file        = File(file, "$name.jpeg")
    return file
}

fun fileExist(name: String, context: Context, valid: Boolean): Boolean {
    val wrapper = ContextWrapper(context)
    var file    = wrapper.getDir("images", Context.MODE_PRIVATE)
    file        = File(file, "$name.jpeg")
    return if(valid && file.exists()) {
        val diff = (Date().time - file.lastModified()) / 1000 / 60
        diff <= 5
    } else {
        file.exists()
    }
}

fun resizeToScreen(image: Bitmap, maxSize: Int): Bitmap {
    var width = image.width
    var height = image.height
    val bitmapRatio = width.toFloat() / height.toFloat()
    if (bitmapRatio > 1) {
        width = maxSize
        height = (width / bitmapRatio).toInt()
    } else {
        height = maxSize
        width = (height * bitmapRatio).toInt()
    }
    return Bitmap.createScaledBitmap(image, width, height, true)
}

fun Bitmap.rotate(degree:Int): Bitmap {
    // Initialize a new matrix
    val matrix = Matrix()

    // Rotate the bitmap
    matrix.postRotate(degree.toFloat())

    // Resize the bitmap
    val scaledBitmap = Bitmap.createScaledBitmap(
        this,
        width,
        height,
        true
    )

    // Create and return the rotated bitmap
    return Bitmap.createBitmap(
        scaledBitmap,
        0,
        0,
        scaledBitmap.width,
        scaledBitmap.height,
        matrix,
        true
    )
}

fun cropToSquare(bitmap: Bitmap): Bitmap {
    val width     = bitmap.width
    val height    = bitmap.height
    val newWidth  = if (height > width) width else height
    val newHeight = if (height > width) height - (height - width) else height
    var cropW     = (width - height) / 2
    cropW         = if (cropW < 0) 0 else cropW
    var cropH     = (height - width) / 2
    cropH         = if (cropH < 0) 0 else cropH
    return Bitmap.createBitmap(bitmap, cropW, cropH, newWidth, newHeight)
}

fun cropAndResizeBitmap(bitmap: Bitmap, maxSize: Int): Bitmap {
    val resized = resizeToScreen(bitmap, maxSize)
    if(resized.width == resized.height) return resized
    return cropToSquare(resized)
}

fun screenMaxSize(context: Context): Int {
    val displayMetrics = context.resources.displayMetrics
    return if(displayMetrics.heightPixels < displayMetrics.widthPixels)
        displayMetrics.heightPixels
    else
        displayMetrics.widthPixels
}

fun screenAvailableSize(context: Context): Int {
    val orientation    = context.resources.configuration.orientation
    val displayMetrics = context.resources.displayMetrics
    return when(orientation) {
        Configuration.ORIENTATION_PORTRAIT -> displayMetrics.widthPixels
        Configuration.ORIENTATION_LANDSCAPE -> {
            displayMetrics.heightPixels - getStatusBarSize(context) - getActionBarSize(context)
        }
        else -> {
            print("Invalid Context"); 0
        }
    }
}

fun getStatusBarSize(context: Context): Int {
    val resource = context.resources.getIdentifier("status_bar_height", "dimen", "android");
    return if(resource>0) {
        context.resources.getDimensionPixelSize(resource)
    } else {
        0
    }
}

fun getActionBarSize(context: Context): Int {
    val tv = TypedValue()
    return if(context.theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
        TypedValue.complexToDimensionPixelSize(tv.data, context.resources.displayMetrics)
    } else {
        0
    }
}

fun bitmapToByteArray(image: Bitmap): ByteArray {
    val stream = ByteArrayOutputStream()
    image.compress(Bitmap.CompressFormat.PNG, 100, stream)
    return stream.toByteArray()
}