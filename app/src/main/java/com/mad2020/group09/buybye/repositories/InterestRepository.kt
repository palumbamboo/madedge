package com.mad2020.group09.buybye.repositories

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.mad2020.group09.buybye.model.Interest

class InterestRepository {
    companion object {
        private const val TAG           = "INTEREST_FIREBASE_REPOSITORY"
        private const val INTEREST_REPO = "Interests"
    }

    private val firestoreDB = FirebaseFirestore.getInstance()

    // Save an interest on the collection
    fun saveInterest(interest: Interest): Task<Void> {
        val interestReference = firestoreDB.collection(INTEREST_REPO).document(interest.id)
        Log.d(TAG, "Added interest ${interest.id} to interests repo")
        return interestReference.set(interest)
    }

    // Get all the interests related to a given item
    fun getInterestsForItem(itemId: String): Query{
        return firestoreDB.collection(INTEREST_REPO).whereEqualTo("itemId", itemId)
    }

    // Delete an interest
    fun deleteInterest(interest: Interest): Task<Void>{
        return firestoreDB.collection(INTEREST_REPO).document(interest.id).delete()
    }

    // Check if a user is interested to a given item
    fun checkInterestForUser(itemId: String, userId: String): Query {
        return firestoreDB.collection(INTEREST_REPO).whereEqualTo("itemId", itemId).whereEqualTo("userId", userId)
    }

    // Get interests by user
    fun getInterestsForUser(userId: String): Query {
        return firestoreDB.collection(INTEREST_REPO).whereEqualTo("userId", userId)
    }

    // Delete interest passing itemId and userId
    fun deleteAssociatedInterest(itemId: String, userId: String): Query {
        var query = firestoreDB.collection(INTEREST_REPO).whereEqualTo("itemId", itemId).whereEqualTo("userId", userId)
        query.get().addOnSuccessListener {
            it.forEach { doc ->
                doc.reference.delete()
            }
        }
        return query
    }
}