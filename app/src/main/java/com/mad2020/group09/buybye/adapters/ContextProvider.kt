package com.mad2020.group09.buybye.adapters

import android.content.Context

interface ContextProvider {
    fun getContext(): Context
}