package com.mad2020.group09.buybye.ui.home

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.adapters.ContextProvider
import com.mad2020.group09.buybye.adapters.MyItemOnSaleAdapter
import com.mad2020.group09.buybye.enums.SortOption
import com.mad2020.group09.buybye.model.Item
import com.mad2020.group09.buybye.repositories.ItemFirebaseViewModel
import com.mad2020.group09.buybye.ui.dialogs.SortDialog
import com.mad2020.group09.buybye.ui.dialogs.TuneDialog
import kotlinx.android.synthetic.main.fragment_on_sale_list.*


class OnSaleListFragment : Fragment(), TuneDialog.DialogListener, SortDialog.DialogListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyItemOnSaleAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var searchView: SearchView

    var data = MutableLiveData<List<Item>>()
    private lateinit var da: List<Item>
    private var categoryFiltered = mutableListOf<Int>()
    private var maxPriceFiltered: Int = 0
    private var sort: String = SortOption.DATE.option
    private var filteringRotation: String = ""

    companion object {
        const val DIALOG_FRAGMENT_SORT = 2
        const val DIALOG_FRAGMENT = 1
        const val ON_SALE_TAG = "OnSale"
        const val TUNE_TAG = "Tune"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_on_sale_list, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.list_menu, menu)

        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.setOnCloseListener { false }

        val searchPlate =
            searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        searchPlate.hint = "Search" // Searching by title, category, subcategory or location
        val searchPlateView: View = searchView.findViewById(androidx.appcompat.R.id.search_plate)
        searchPlateView.setBackgroundColor(
            ContextCompat.getColor(
                requireActivity().applicationContext,
                android.R.color.transparent
            )
        )

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                // method called after the user clicks the enter/search after inputting the search query

                chipGroup.removeAllViews()
                categoryFiltered = mutableListOf<Int>()
                maxPriceFiltered = 0

                viewAdapter.filter.filter(ON_SALE_TAG + "_" + query)
                if (viewAdapter.itemCount == 0)
                    Toast.makeText(requireContext(), "No match found.", Toast.LENGTH_SHORT).show()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // method used when the change occurs in the query field
                chipGroup.removeAllViews()
                categoryFiltered = mutableListOf<Int>()
                maxPriceFiltered = 0

                viewAdapter.filter.filter(ON_SALE_TAG + "_" + newText)

                return false
            }
        })

        val tuneItem: MenuItem = menu.findItem(R.id.action_tune)
        tuneItem.setOnMenuItemClickListener {
            val fragmentTransaction = parentFragmentManager.beginTransaction()
            val prev = parentFragmentManager.findFragmentByTag("dialog")
            if (prev != null) {
                fragmentTransaction.remove(prev)
            }

            val tuneFragment = TuneDialog()
            tuneFragment.setTargetFragment(this, DIALOG_FRAGMENT)
            val bundle = Bundle()
            if (categoryFiltered.isNotEmpty()) {
                val cats = arrayListOf<Int>()
                for (cat in categoryFiltered)
                    cats.add(cat)
                bundle.putIntegerArrayList("categories", cats)
            }
            bundle.putInt("price", maxPriceFiltered)
            tuneFragment.arguments = bundle
            tuneFragment.show(fragmentTransaction, "dialog")

            true
        }

        val sortItem: MenuItem = menu.findItem(R.id.sort_tune)
        sortItem.setOnMenuItemClickListener {
            val fragmentTransaction = parentFragmentManager.beginTransaction()
            val prev = parentFragmentManager.findFragmentByTag("dialog")
            if (prev != null) {
                fragmentTransaction.remove(prev)
            }

            val tuneFragment = SortDialog()
            tuneFragment.setTargetFragment(this, DIALOG_FRAGMENT_SORT)

            val bundle = Bundle()
            bundle.putString("sort", sort)
            tuneFragment.arguments = bundle

            tuneFragment.show(fragmentTransaction, "dialog")

            true
        }

        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            DIALOG_FRAGMENT -> {
                if (resultCode == Activity.RESULT_OK) {
                    // After Ok code.
                    setTuning()
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // After Cancel code.
                }
            };
            DIALOG_FRAGMENT_SORT -> {
                if (resultCode == Activity.RESULT_OK) {

                    reloadSortedItem(sort)

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // After Cancel code.
                }
            }
        }
    }

    private fun setTuning() {
        chipGroup.removeAllViews()
        var strCategories = arrayListOf<String>()
        if (categoryFiltered.isNotEmpty()) {
            for (cat in categoryFiltered) {
                val chip = Chip(chipGroup.context)

                when (cat) {
                    1 -> chip.text = "Arts & Crafts"
                    2 -> chip.text = "Sports & Hobby"
                    3 -> chip.text = "Baby"
                    4 -> chip.text = "Women's fashion"
                    5 -> chip.text = "Men's fashion"
                    6 -> chip.text = "Electronics"
                    7 -> chip.text = "Games & Videogames"
                    8 -> chip.text = "Automotive"
                }

                // necessary to get single selection working

                strCategories.add(chip.text.toString())
                chip.isClickable = false
                chip.isCheckable = false
                chip.visibility = View.VISIBLE
                chipGroup.addView(chip)

            }
        }

        if (maxPriceFiltered != 0) {
            val chip = Chip(chipGroup.context)
            chip.text = "0 - $maxPriceFiltered €"
            chip.isClickable = false
            chip.isCheckable = false
            chip.visibility = View.VISIBLE
            chipGroup.addView(chip)

        }
        var filterString = ""
        if (strCategories.isNullOrEmpty()) {
            filterString = TUNE_TAG + "_" + maxPriceFiltered.toString()

        } else {
            filterString =
                TUNE_TAG + "_" + maxPriceFiltered.toString() + "_" + strCategories.joinToString(
                    "|"
                )

        }
        filteringRotation = filterString
        Log.d("FILTER_TUNE", filterString)
        viewAdapter.getFilter().filter(filterString)
        //if (viewAdapter.itemCount == 0)
        //Toast.makeText(requireContext(), "No match found.", Toast.LENGTH_SHORT).show()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (sort != SortOption.DATE.option) {
            outState.putString("sortOption", sort)
        }
        if (filteringRotation != "") {
            outState.putString("filterOption", filteringRotation)
            outState.putIntArray("filterCategory", categoryFiltered.toIntArray())
            outState.putInt("filterPrice", maxPriceFiltered)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showProgressBar()
        txtEmptyList.visibility = View.INVISIBLE


        var tmpSort = savedInstanceState?.getString("sortOption")
        var tmpFiltering = savedInstanceState?.getString("filterOption")
        var tmpCategory = savedInstanceState?.getIntArray("filterCategory")
        var tmpPrice = savedInstanceState?.getInt("filterPrice")

        if (tmpSort != null && tmpSort!= SortOption.DATE.option) {
            sort = tmpSort
        }

        if (tmpFiltering != null && tmpFiltering != "") {
            filteringRotation = tmpFiltering
        }

        if (tmpCategory != null && tmpCategory.isNotEmpty()) {
            categoryFiltered = tmpCategory.toMutableList()
        }

        if (tmpPrice != null && tmpPrice != 0) {
            maxPriceFiltered = tmpPrice
        }

        val itemRepo = ViewModelProvider(this).get(ItemFirebaseViewModel::class.java)
        itemRepo.getSavedItems().observe(viewLifecycleOwner, Observer {
            data.value = it.sortedBy { item -> item.creationDate }.reversed()
            when (sort) {
                SortOption.LOWERPRICE.option -> data.value = it.sortedBy { item -> item.price }
                SortOption.HIGHERPRICE.option -> data.value = it.sortedBy { item -> item.price }.reversed()
            }
            da = it!!

            val currentUser = Firebase.auth.currentUser
            if (currentUser != null && currentUser.email != null) {
                val email: String = currentUser.email!!

                data.value = data.value!!.filter { item ->
                    item.sellerEmail != email && item.validForSale()
                }
            } else {
                data.value = data.value!!.filter { item -> item.validForSale() }
            }

            viewManager = LinearLayoutManager(this.context)

            viewAdapter = MyItemOnSaleAdapter(
                data,
                viewLifecycleOwner,
                object : ContextProvider {
                    override fun getContext(): Context {
                        return activity!!.applicationContext
                    }
                }
            )

            recyclerView = requireView().findViewById<RecyclerView>(R.id.my_recycler_view).apply {
                // use a linear layout manager
                layoutManager = viewManager
                // specify an viewAdapter
                adapter = viewAdapter
            }

            if (data.value.isNullOrEmpty()) {
                txtEmptyList.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
            } else {
                txtEmptyList.visibility = View.INVISIBLE
                recyclerView.visibility = View.VISIBLE
            }

            // Filtering after rotation
            if (filteringRotation != "") {
                setTuning()
            }

            hideProgressBar()
        })
    }

    private fun showProgressBar() {
        progressBarOnSaleList.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBarOnSaleList.visibility = View.INVISIBLE
    }

    override fun onFinishEditDialog(inputCat: MutableList<Int>, minPrice: Int, maxPrice: Int) {
        // should call the adapter filter that receives tags
        categoryFiltered = mutableListOf()
        for (cat in inputCat)
            categoryFiltered.add(cat)
        maxPriceFiltered = if (minPrice == maxPrice) {
            0
        } else {
            maxPrice
        }
    }

    override fun onFinishEditSortDialog(sort: String) {
        this.sort = sort
    }

    private fun reloadSortedItem(sort: String) {

        when (sort) {
            SortOption.DATE.option -> {
                viewAdapter.filterItemDataset.value =
                    viewAdapter.filterItemDataset.value!!.sortedBy { item -> item.creationDate }
                        .reversed()
            }
            SortOption.LOWERPRICE.option -> {
                viewAdapter.filterItemDataset.value =
                    viewAdapter.filterItemDataset.value!!.sortedBy { item -> item.price }
            }
            SortOption.HIGHERPRICE.option -> {
                viewAdapter.filterItemDataset.value =
                    viewAdapter.filterItemDataset.value!!.sortedBy { item -> item.price }.reversed()
            }
        }

        viewAdapter.notifyDataSetChanged()
    }

}
