package com.mad2020.group09.buybye.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.EventListener
import com.mad2020.group09.buybye.model.Interest

class InterestViewModel: ViewModel() {
    companion object {
        private const val TAG = "INTEREST_FIREBASE_VIEW_MODEL"
    }

    private val interestRepository = InterestRepository()
    var savedInterests: MutableLiveData<List<Interest>> = MutableLiveData()
    var associatedInterest: MutableLiveData<Interest> = MutableLiveData()

    // Save interest
    fun saveInterestToFirebase(interest: Interest){
        interestRepository.saveInterest(interest).addOnFailureListener{
            Log.e(TAG, "SAVE FAILED on Interest: ${it.message}")
        }.addOnSuccessListener {
            Log.i(TAG, "SAVE DONE on Interest: interest ${interest.id}")
        }
    }

    // Get all interests related to an item
    fun getInterestsForItem(itemId: String): LiveData<List<Interest>>{
        interestRepository.getInterestsForItem(itemId).addSnapshotListener(EventListener {value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Interests: ${e.message}", e)
                savedInterests.value = null
                return@EventListener
            }

            val savedInterestsList: MutableList<Interest> = mutableListOf()
            for(doc in value!!){
                val interest = doc.toObject(Interest::class.java)
                savedInterestsList.add(interest)
            }
            savedInterests.value = savedInterestsList
        })
        return savedInterests
    }

    // Delete interest
    fun deleteInterest(interest: Interest){
        interestRepository.deleteInterest(interest).addOnFailureListener{
            Log.e(TAG, "DELETE FAILED on Interest: ${it.message}")
        }
    }

    // Check if user is interested to an item
    fun checkUserInterest(itemId: String, userId: String): LiveData<Interest>{
        interestRepository.checkInterestForUser(itemId, userId).addSnapshotListener(EventListener {value, e ->
            if (e != null) {
                Log.e(TAG, "CHECK USER-INTEREST FAILED on Interests: ${e.message}", e)
                associatedInterest.value = null
                return@EventListener
            }

            if(value!!.isEmpty)
                associatedInterest.value = null
            else
                associatedInterest.value = value!!.first().toObject(Interest::class.java)
        })
        return associatedInterest
    }

    // Check if user is interested to an item
    fun getInterestsForUser(userId: String): LiveData<List<Interest>>{
        val interestsForUser: MutableLiveData<List<Interest>> = MutableLiveData()
        interestRepository.getInterestsForUser(userId).addSnapshotListener(EventListener {value, e ->
            if (e != null) {
                Log.e(TAG, "CHECK USER-INTEREST FAILED on Interests: ${e.message}", e)
                interestsForUser.value = null
                return@EventListener
            }

            val savedInterestsList: MutableList<Interest> = mutableListOf()
            for(doc in value!!){
                val interest = doc.toObject(Interest::class.java)
                savedInterestsList.add(interest)
            }
            interestsForUser.value = savedInterestsList
        })
        return interestsForUser
    }

    // Delete an associated interest
    fun deleteAssociatedInterest(itemId: String, userId: String){
        interestRepository.deleteAssociatedInterest(itemId, userId)
    }
}