package com.mad2020.group09.buybye.ui.dialogs

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.adapters.ContextProvider
import com.mad2020.group09.buybye.adapters.MyInterestedUserProfileNoLivedata
import com.mad2020.group09.buybye.model.UserProfile

class BuyerChooserDialog : DialogFragment(), LifecycleObserver {

    private lateinit var inflater: LayoutInflater
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyInterestedUserProfileNoLivedata
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val selectedUser: MutableLiveData<UserProfile> = MutableLiveData()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        inflater = requireActivity().layoutInflater
        val builder = AlertDialog.Builder(requireContext())
        val fragment = inflater.inflate(R.layout.fragment_buyer_chooser, null)
        builder.setView(fragment)
        builder.setTitle("Select buyer")
        builder.setNegativeButton(
            "Cancel"
        ) { _, _ -> dismiss() }

        val usersList = requireArguments().getParcelableArrayList<UserProfile>("users")!!.toList()
        val usersData = MutableLiveData(usersList)

        viewManager = LinearLayoutManager(requireContext())
        viewAdapter = MyInterestedUserProfileNoLivedata(
            usersData.value!!,
            object : ContextProvider {
                override fun getContext(): Context {
//                    return activity!!.applicationContext
                    return requireContext()
                }
            },
            selectedUser
        )
        recyclerView =
            fragment.findViewById<RecyclerView>(R.id.userInterestedRecyclerView)
                .apply {
                    // use a linear layout manager
                    layoutManager = viewManager
                    // specify an viewAdapter (see also next example)
                    adapter = viewAdapter
                }

        selectedUser.observe(this, Observer {
            val dialogListener = targetFragment!! as BuyerChooserDialog.DialogListener
            dialogListener.onFinishEditDialog(it)
            dismiss()
            targetFragment!!.onActivityResult(
                targetRequestCode,
                Activity.RESULT_OK,
                requireActivity().intent
            )
        })

        return builder.create()
    }

    interface DialogListener {
        fun onFinishEditDialog(buyer: UserProfile?)
    }
}