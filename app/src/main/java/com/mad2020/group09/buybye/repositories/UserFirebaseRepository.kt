package com.mad2020.group09.buybye.repositories

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.mad2020.group09.buybye.model.UserProfile

class UserFirebaseRepository {

    companion object {
        private const val TAG       = "USER_FIREBASE_REPOSITORY"
        private const val USER_REPO = "Users"
    }

    private val firestoreDB = FirebaseFirestore.getInstance()

    fun saveUser(user: UserProfile): Task<Void> {
        val userReference = firestoreDB.collection(USER_REPO).document(user.id)
        Log.d(TAG, "Added user ${user.id} to users repo")
        return userReference.set(user)
    }

    fun getUser(userId: String): DocumentReference {
        return firestoreDB.collection(USER_REPO).document(userId)
    }

    fun getSavedUsers(): CollectionReference {
        return firestoreDB.collection(USER_REPO)
    }

    fun getSavedUsersByIds(userIds: List<String>): Query {
        return firestoreDB.collection(USER_REPO).whereIn("id", userIds)
    }

    fun deleteUser(user: UserProfile): Task<Void> {
        return firestoreDB.collection(USER_REPO).document(user.id).delete()
    }

    fun getUserByEmail(email: String): Query {
        return firestoreDB.collection("Users").whereEqualTo("email", email)
    }

}