package com.mad2020.group09.buybye.utilities

import androidx.lifecycle.LiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.UserFirebaseViewModel

private lateinit var currentUserProfile: LiveData<UserProfile>
private val userRepo = UserFirebaseViewModel()

/**
 * Load user from local file system
 */
fun getCurrentUser(): LiveData<UserProfile> {
    return if(::currentUserProfile.isInitialized)
        currentUserProfile
    else {
        val firebaseUser = Firebase.auth.currentUser
        userRepo.getSavedUserByEmail(firebaseUser!!.email!!)
    }
}

fun userExist(): Boolean {
    return Firebase.auth.currentUser != null
}