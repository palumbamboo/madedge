package com.mad2020.group09.buybye.ui.dialogs

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.enums.SortOption
import com.mad2020.group09.buybye.ui.home.OnSaleListFragment
import kotlinx.android.synthetic.main.fragment_tune_dialog.*


class SortDialog : DialogFragment() {

    private lateinit var inflater: LayoutInflater
    private var sort: String = SortOption.DATE.option

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        inflater = requireActivity().layoutInflater
        val builder = AlertDialog.Builder(requireContext())
        val v = inflater.inflate(R.layout.fragment_sort_dialog, null)
        builder.setView(v)
        builder.setPositiveButton(
            "Order"
        ) { dialog, which ->
            val dialogListener = targetFragment!! as DialogListener
            dialogListener.onFinishEditSortDialog(sort)
            dismiss()
            targetFragment!!.onActivityResult(
                targetRequestCode,
                Activity.RESULT_OK,
                requireActivity().intent
            )
        }
        builder.setNegativeButton(
            "Cancel"
        ) { _, _ -> dismiss() }

        v.findViewById<RadioButton>(R.id.radio1).setOnClickListener {
            onRadioClicked(it, "date")
        }
        v.findViewById<RadioButton>(R.id.radio2).setOnClickListener {
            onRadioClicked(it, "lowerPrice")
        }
        v.findViewById<RadioButton>(R.id.radio3).setOnClickListener {
            onRadioClicked(it, "higherPrice")
        }

        if (arguments != null) {
            val sortArgument = arguments?.getString("sort")
            if (sortArgument != null) {
                when (sortArgument) {
                    SortOption.DATE.option -> v.findViewById<RadioButton>(R.id.radio1).isChecked =
                        true
                    SortOption.LOWERPRICE.option -> v.findViewById<RadioButton>(R.id.radio2).isChecked =
                        true
                    SortOption.HIGHERPRICE.option -> v.findViewById<RadioButton>(R.id.radio3).isChecked =
                        true
                }
            }
        }

        return builder.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var setFullScreen = false
        if (arguments != null) {
            setFullScreen = requireNotNull(arguments?.getBoolean("fullScreen"))
        }
        if (setFullScreen)
            setStyle(STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
    }

    interface DialogListener {
        fun onFinishEditSortDialog(sort: String)
    }

    private fun onRadioClicked(view: View, sortChosen: String) {
        if (view is RadioButton) {
            val checked: Boolean = view.isChecked

            sort = if (checked) {
                sortChosen
            } else {
                SortOption.DATE.option
            }
        }
    }
}