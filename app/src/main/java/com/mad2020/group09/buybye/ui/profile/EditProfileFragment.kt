package com.mad2020.group09.buybye.ui.profile

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.UiSettings
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.navigation.NavigationView
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.enums.KeyCollector
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.ImageRepository
import com.mad2020.group09.buybye.repositories.UserFirebaseViewModel
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import com.mad2020.group09.buybye.utilities.*
import kotlinx.android.synthetic.main.fragment_edit_profile.scrollView
import kotlinx.android.synthetic.main.fragment_item_edit.*
import org.json.JSONObject

class EditProfileFragment : Fragment() {

    companion object {
        //Permission code gallery
        private const val PERMISSION_CODE_GALLERY = 1000
        //image pick code
        private const val IMAGE_PICK_CODE = 1001
        //Permission code camera
        private const val PERMISSION_CODE_CAMERA = 1002
        //image capture code
        private const val IMAGE_CAPTURE_CODE = 1003

        var image_uri: Uri?           = null
        var tempImageProfile: Bitmap? = null
        var screenSize                = 800
        var availableSize             = 800
        var userHasPhoto: Boolean     = false
        var isMarkerPresent           = false
    }

    private lateinit var userProfile: UserProfile

    private lateinit var googleMap: GoogleMap
    private var userLatitude: Double = 0.0
    private var userLongitude: Double = 0.0
    private var queryLat: Double = 0.0
    private var queryLng: Double = 0.0
    private lateinit var currPosition: LatLng
    private var userCity: String = ""
    private var userProvince: String = ""
    private var userCountry: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.edit_profile_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.save_user -> {
                saveProfile()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putDouble("userLatitude", userLatitude)
        outState.putDouble("userLongitude", userLongitude)

        tempImageProfile?.let {
            outState.putByteArray("tempImageProfile", bitmapToByteArray(it))
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        savedInstanceState?.let {

            savedInstanceState.getByteArray("tempImageProfile")?.let { image ->
                tempImageProfile = BitmapFactory.decodeByteArray(image, 0, image.size)
                userProfileImage.setImageBitmap(
                    cropAndResizeBitmap(
                        tempImageProfile!!,
                        availableSize
                    )
                )
            }

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let {
            screenSize = screenMaxSize(it)
            availableSize = screenAvailableSize(it)
        }

        tempImageProfile = null
        userHasPhoto     = false

        userProfile = arguments?.getParcelable("userProfile")!!
        var userImage: Bitmap? = null
        arguments?.getBoolean("userProfileImage")?.let {
            if(it)
                userImage = readBitmapFromStorageMemory(userProfile.photoId, requireContext())
        }

        if(userImage != null)
            populateXml(userProfile, userImage)
        else
            populateXml(userProfile, null)

        editUserProfilePhone.addTextChangedListener(PhoneNumberFormattingTextWatcher())

        // Map initialization
        editUserMapView.onCreate(savedInstanceState)
        editUserMapView.onResume()
        editUserMapView.getMapAsync {
            googleMap = it
            googleMap.uiSettings.isZoomControlsEnabled = true
            // Ask for location permissions
            while(ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                requestPermissions()
            }
            googleMap.isMyLocationEnabled = true

            // Check for updated marker after screen rotation
            var passedLatitude = savedInstanceState?.getDouble("userLatitude")
            var passedLongitude = savedInstanceState?.getDouble("userLongitude")
            if(passedLatitude != null && passedLongitude != null && passedLatitude != 0.0 && passedLongitude != 0.0){
                userProfile.latitude = passedLatitude
                userProfile.longitude = passedLongitude
            }

            // Place search
            editUserSearchLocation.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }

                override fun onQueryTextSubmit(query: String?): Boolean {
                    var cleanQuery = query?.replace(" ", "%20")
                    val urlLocation = "https://maps.googleapis.com/maps/api/geocode/json?address=${cleanQuery}&key=${KeyCollector.GEOCODING.key}"
                    val locationRequest = object : StringRequest(Request.Method.GET, urlLocation, Response.Listener<String> {
                            response ->
                        val jsonResponse = JSONObject(response)
                        // Get routes
                        val results = jsonResponse.getJSONArray("results")
                        val geometry = results.getJSONObject(0).getJSONObject("geometry")
                        val location = geometry.getJSONObject("location")
                        queryLat = location.getDouble("lat")
                        queryLng = location.getDouble("lng")
                        if(queryLat != 0.0 && queryLng != 0.0){
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(queryLat, queryLng), 12.0f))
                        }

                    }, Response.ErrorListener {
                            _ ->
                    }){}
                    val requestQueue = Volley.newRequestQueue(requireContext())
                    requestQueue.add(locationRequest)
                    return false
                }
            })

            // Retrieve location's coordinates, and move camera there if a position has not already been marked
            FusedLocationProviderClient(requireActivity()).lastLocation.addOnCompleteListener { loc ->
                currPosition = LatLng(loc.result!!.latitude, loc.result!!.longitude)

                if(!isMarkerPresent){
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currPosition, 16.0f)) // Moving map to position
                }
            }

            // A marker was already assigned to the item, checking also that latitude and longitude are not null for the previous elements already stored on Firestore
            if(userProfile.latitude != null && userProfile.longitude != null && userProfile.latitude != 0.0 && userProfile.longitude != 0.0){
                googleMap.addMarker(MarkerOptions().position(LatLng(userProfile.latitude, userProfile.longitude)))
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(userProfile.latitude, userProfile.longitude), 16.0f)) // Moving map to position
                isMarkerPresent = true
            }

            // Marker management
            googleMap.setOnMapClickListener {pos ->
                var markerOptions = MarkerOptions()
                markerOptions.position(pos)
                if(!isMarkerPresent) {
                    //No marker yet in the map
                    googleMap.addMarker(markerOptions)
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 16.0f)) // Moving map to marker
                    isMarkerPresent = true
                }
                else {
                    // Delete old marker
                    googleMap.clear()
                    googleMap.addMarker(markerOptions)
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 16.0f)) // Moving map to marker
                }
                userLatitude = markerOptions.position.latitude
                userLongitude = markerOptions.position.longitude

                // Fetch city and country
                val urlDirections = "https://maps.googleapis.com/maps/api/geocode/json?latlng=${userLatitude},${userLongitude}&key=${KeyCollector.GEOCODING.key}"
                val directionsRequest = object : StringRequest(Request.Method.GET, urlDirections, Response.Listener<String> {
                        response ->
                    val jsonResponse = JSONObject(response)
                    // Get routes
                    val results = jsonResponse.getJSONArray("results")
                    val addressComponents = results.getJSONObject(0).getJSONArray("address_components")
                    val size = addressComponents.length()
                    userCity = if (size >= 3) addressComponents.getJSONObject(2)
                        .getString("short_name") else "No Data"
                    userProvince = if (size >= 5) addressComponents.getJSONObject(4)
                        .getString("short_name") else "No Data"
                    userCountry = if (size >= 7) addressComponents.getJSONObject(6)
                        .getString("long_name") else "No Data"
                    var isPostalCode = true
                    try {
                        val num = java.lang.Double.parseDouble(userCountry)
                    } catch (e: NumberFormatException) {
                        isPostalCode = false
                    }
                    if(isPostalCode){
                        userCountry = if (size >= 7) addressComponents.getJSONObject(5)
                            .getString("long_name") else "No Data"
                    }

                }, Response.ErrorListener {
                        _ ->
                }){}
                val requestQueue = Volley.newRequestQueue(requireContext())
                requestQueue.add(directionsRequest)
            }
        }

        // set click listener on rotate button
        userProfileImageRotateButton.setOnClickListener {
            if (tempImageProfile == null && !userHasPhoto) {
                Toast.makeText(
                    requireContext(),
                    "Please upload a photo to rotate it.",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (tempImageProfile != null) {
                tempImageProfile = tempImageProfile!!.rotate(90)
                userProfileImage.setImageBitmap(
                    cropAndResizeBitmap(
                        tempImageProfile!!,
                        availableSize
                    )
                )
            } else {
                tempImageProfile = userProfileImage.drawable.toBitmap()
                tempImageProfile = tempImageProfile!!.rotate(90)
                userProfileImage.setImageBitmap(
                    cropAndResizeBitmap(
                        tempImageProfile!!,
                        availableSize
                    )
                )
            }
        }

        // set click listener on edit photo button
        userProfileImageEditButton.setOnClickListener { v ->
            val builder = v?.context?.let {
                AlertDialog.Builder(it)
            }

            builder?.setTitle("Select a new image")
            builder?.setItems(
                arrayOf("from Camera", "from Gallery")
            ) { _, which ->
                if (which == 0) {
                    if (requireActivity().checkSelfPermission(Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED ||
                        requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED
                    ) {
                        //permission was not enabled
                        val permission = arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        //show popup to request permission
                        requestPermissions(permission, PERMISSION_CODE_CAMERA)
                    } else {
                        pickImageFromCamera()
                    }
                } else if (which == 1) {
                    if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_DENIED
                    ) {
                        //permission denied
                        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                        //show popup to request runtime permission
                        requestPermissions(permissions, PERMISSION_CODE_GALLERY)
                    } else {
                        //permission already granted
                        pickImageFromGallery()
                    }
                }
            }

            builder?.create()?.show()
        }

        editUserMapViewTransparent.setOnTouchListener { view, event ->
            view.performClick()
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    scrollView.requestDisallowInterceptTouchEvent(true)
                    false
                }
                MotionEvent.ACTION_UP -> {
                    scrollView.requestDisallowInterceptTouchEvent(false)
                    true
                }
                MotionEvent.ACTION_MOVE -> {
                    scrollView.requestDisallowInterceptTouchEvent(true)
                    false
                }
                else -> true
            }
        }


    }

    /**
     * Private method to return data to ShowActivity
     */
    private fun saveProfile() {
        val userRepo = ViewModelProvider(this).get(UserFirebaseViewModel::class.java)
        if (editUserProfileName.text.isNotEmpty() &&
            editUserProfileNickname.text.isNotEmpty() &&
            editUserProfileEmail.text.isNotEmpty() &&
            editUserProfilePhone.text.isNotEmpty()
        ) {
            userProfile.name = editUserProfileName.text.toString()
            userProfile.nickname = editUserProfileNickname.text.toString()
            userProfile.prefix = editUserProfilePrefix.text.toString()
            userProfile.phone = editUserProfilePhone.text.toString()
            if(userLatitude != 0.0 && userLongitude != 0.0 && isMarkerPresent){
                userProfile.latitude = userLatitude
                userProfile.longitude = userLongitude
            }
            if(userCity.isNotEmpty() && userProvince.isNotEmpty() && userCountry.isNotEmpty()){
                userProfile.location = "${userCity} (${userProvince})"
                userProfile.country = "${userCountry}"
            }

            userRepo.saveUserToFirebase(userProfile)
            val navView: NavigationView = requireActivity().findViewById(R.id.nav_view)
            val headerView = navView.getHeaderView(0)
            headerView.findViewById<TextView>(R.id.headerTitle).text = userProfile.nickname
            headerView.findViewById<TextView>(R.id.headerDescription).text = userProfile.email

            val bundle = Bundle()
            bundle.putParcelable("userProfileUpdated", userProfile)

            tempImageProfile?.let { bitmap ->
                ImageRepository().uploadUserImage(userProfile.photoId, bitmap)
                writeBitmapToStorageMemory(bitmap, openFile(userProfile.photoId, requireActivity()))
                headerView.findViewById<ImageView>(R.id.headerImage).setImageBitmap(
                    cropAndResizeBitmap(bitmap, 208))
                bundle.putBoolean("userImageUpdated", true)
            }


            val imm =
                requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(requireView().windowToken, 0)

            findNavController().navigate(R.id.nav_profile, bundle)
        } else {
            Toast.makeText(
                context,
                "Some fields are missing. Please insert them.",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    /**
     * invoke an intent to a camera app to retrieve a new image
     */
    private fun pickImageFromCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = requireActivity().contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    /**
     * invoke an intent to a photo gallery app to retrieve a saved image
     */
    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    /**
     * handle requested permission result
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE_CAMERA -> {
                if (grantResults.isEmpty() || (grantResults.isNotEmpty() && grantResults[0] !=
                            PackageManager.PERMISSION_GRANTED
                            && grantResults[1] != PackageManager.PERMISSION_GRANTED)
                ) {
                    //permission from popup denied
                    Toast.makeText(
                        requireActivity().applicationContext,
                        "Permission denied",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    pickImageFromCamera()
                }
            }
            PERMISSION_CODE_GALLERY -> {
                if (grantResults.isEmpty() || (grantResults.isNotEmpty() && grantResults[0] !=
                            PackageManager.PERMISSION_GRANTED)
                ) {
                    //permission from popup denied
                    Toast.makeText(
                        requireActivity().applicationContext,
                        "Permission denied",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    pickImageFromGallery()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var newBitmap: Bitmap? = null

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_CAPTURE_CODE) {
            Toast.makeText(
                requireActivity().applicationContext,
                "Profile image updated",
                Toast.LENGTH_SHORT
            ).show()
            newBitmap = if (Build.VERSION.SDK_INT < 28) {
                MediaStore.Images.Media.getBitmap(
                    requireActivity().contentResolver,
                    image_uri
                )
            } else {
                val source = image_uri?.let {
                    ImageDecoder.createSource(
                        requireActivity().contentResolver,
                        it
                    )
                }
                source?.let { ImageDecoder.decodeBitmap(it) }
            }
        }

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            Toast.makeText(
                requireActivity().applicationContext,
                "Profile image updated",
                Toast.LENGTH_SHORT
            ).show()
            val imageView = ImageView(requireActivity().applicationContext)
            imageView.setImageURI(data?.data)
            newBitmap = (imageView.drawable as BitmapDrawable).bitmap
        }

        tempImageProfile = newBitmap?.let { cropAndResizeBitmap(it, screenSize) }
        tempImageProfile?.let {
            userProfileImage.setImageBitmap(
                cropAndResizeBitmap(
                    it,
                    availableSize
                )
            )
        }
    }

    private fun populateXml(user: UserProfile, image: Bitmap?) {
        editUserProfileName.setText(user.name)
        editUserProfileNickname.setText(user.nickname)
        editUserProfileEmail.text = user.email
        editUserSearchLocation.setQuery(user.location, false)
        editUserProfilePrefix.setText(user.prefix)
        editUserProfilePhone.setText(user.phone)

        if (image != null) {
            userProfileImage.setImageBitmap(
                cropAndResizeBitmap(
                    image,
                    screenAvailableSize(requireContext())
                )
            )
            userHasPhoto = true
        }
    }

    /*
        Request location permissions to user
     */
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            42
        )
        // Request code 42 is for the location
    }

}
