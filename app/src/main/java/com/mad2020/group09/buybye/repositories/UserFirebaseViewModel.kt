package com.mad2020.group09.buybye.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.EventListener
import com.mad2020.group09.buybye.model.UserProfile

class UserFirebaseViewModel : ViewModel() {

    companion object {
        private const val TAG = "USER_FIREBASE_VIEW_MODEL"
    }

    private val userRepository                          = UserFirebaseRepository()
    var savedUsers : MutableLiveData<List<UserProfile>> = MutableLiveData()
    var savedUser  : MutableLiveData<UserProfile>       = MutableLiveData()

    fun saveUserToFirebase(user: UserProfile){
        userRepository.saveUser(user).addOnFailureListener {
            Log.e(TAG, "SAVE FAILED on User: ${it.message}")
        }
    }

    fun getSavedUser(userId: String): LiveData<UserProfile> {
        userRepository.getUser(userId).addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                savedUser.value = null
                return@EventListener
            }
            val userDb = value?.toObject(UserProfile::class.java)
            userDb?.id = value?.id.toString()
            savedUser.value = userDb
        })
        return savedUser
    }

    fun getSavedUsers(): LiveData<List<UserProfile>> {
        userRepository.getSavedUsers().addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Users: ${e.message}", e)
                savedUsers.value = null
                return@EventListener
            }

            val savedUserList: MutableList<UserProfile> = mutableListOf()
            for(doc in value!!) {
                val newUser = doc.toObject(UserProfile::class.java)
                savedUserList.add(newUser)
            }
            savedUsers.value = savedUserList
        })
        return savedUsers
    }

    fun getSavedUsersByIds(userIds: List<String>): LiveData<List<UserProfile>> {
        val savedUsersByIds: MutableLiveData<List<UserProfile>> = MutableLiveData()
        userRepository.getSavedUsersByIds(userIds).addSnapshotListener(EventListener { value, e ->
            if (e != null) {
                Log.e(TAG, "LISTEN FAILED on Users byIds: ${e.message}", e)
                savedUsersByIds.value = null
                return@EventListener
            }

            Log.d(TAG, "LISTEN DONE on User by userIds: loaded ${value!!.size()}")

            val savedUsers: MutableList<UserProfile> = mutableListOf()
            for(doc in value) {
                val user = doc.toObject(UserProfile::class.java)
                savedUsers.add(user)
            }
            savedUsersByIds.value = savedUsers
        })
        return savedUsersByIds
    }

    fun deleteUser(user: UserProfile) {
        userRepository.deleteUser(user).addOnFailureListener {
            Log.e(TAG, "DELETE FAILED on User: ${it.message}")
        }
    }

    fun getSavedUserByEmail(email: String): LiveData<UserProfile> {
        val user: MutableLiveData<UserProfile> = MutableLiveData()
        userRepository.getUserByEmail(email).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                if (task.result!!.size() != 0) {
                    val document = task.result!!.documents[0]
                    user.value = document.toObject(UserProfile::class.java)
                } else {
                    Log.d(TAG, "GET EMPTY on User by email: no user with such email ${email}")
                    user.value = null
                }
            } else {
                Log.e(TAG, "GET FAILED on User by email: ", task.exception)
            }
        }
        return user
    }

}