package com.mad2020.group09.buybye.enums

enum class SortOption(val option: String) {
    DATE("date"),
    LOWERPRICE("lowerPrice"),
    HIGHERPRICE("higherPrice"),
}