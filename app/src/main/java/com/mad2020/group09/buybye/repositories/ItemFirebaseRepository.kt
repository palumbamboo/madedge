package com.mad2020.group09.buybye.repositories

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.mad2020.group09.buybye.enums.ItemStatus
import com.mad2020.group09.buybye.model.Item

class ItemFirebaseRepository {

    companion object {
        private const val TAG       = "ITEM_FIREBASE_REPOSITORY"
        private const val ITEM_REPO = "Items"
    }

    private val firestoreDB = FirebaseFirestore.getInstance()

    fun saveItem(item: Item): Task<Void> {
        val itemReference = firestoreDB.collection(ITEM_REPO).document(item.id)
        Log.d(TAG, "Added item ${item.id} to items repo")
        return itemReference.set(item)
    }

    fun getItem(itemId: String): DocumentReference {
        return firestoreDB.collection(ITEM_REPO).document(itemId)
    }

    fun getSavedItems(): CollectionReference {
        return firestoreDB.collection(ITEM_REPO)
    }

    fun getSavedItemsByUser(userId: String): Query {
        val itemReference = firestoreDB.collection(ITEM_REPO)
        return itemReference.whereEqualTo("sellerId", userId)
    }

    fun getItemsToConfirmByUser(userId: String): Query {
        val itemReference = firestoreDB.collection(ITEM_REPO)
        val items = itemReference.whereEqualTo("buyerId", userId)
        return items.whereEqualTo("status", ItemStatus.WAITING_CONFIRMATION.status)
    }

    fun getSavedItemsByIds(itemIds: List<String>): Query {
        val itemReference = firestoreDB.collection(ITEM_REPO)
        return itemReference.whereIn("id", itemIds)
    }

    fun getSavedItemsByBuyer(buyerId: String): Query {
        val itemReference = firestoreDB.collection(ITEM_REPO)
        val items = itemReference.whereEqualTo("buyerId", buyerId)
        return items.whereEqualTo("status", ItemStatus.SOLD.status)
    }

    fun deleteItem(item: Item): Task<Void> {
        return firestoreDB.collection(ITEM_REPO).document(item.id).delete()
    }

}