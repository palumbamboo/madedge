package com.mad2020.group09.buybye.ui.profile

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.adapters.ContextProvider
import com.mad2020.group09.buybye.adapters.MyReviewsOnProfileAdapter
import com.mad2020.group09.buybye.model.Item
import com.mad2020.group09.buybye.model.Review
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.ImageRepository
import com.mad2020.group09.buybye.repositories.ReviewViewModel
import com.mad2020.group09.buybye.repositories.UserFirebaseViewModel
import com.mad2020.group09.buybye.utilities.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_item_details.*
import kotlinx.android.synthetic.main.fragment_show_profile.*
import kotlinx.android.synthetic.main.fragment_show_profile.scrollView
import kotlinx.android.synthetic.main.fragment_show_profile.userProfileImage


class ShowProfileFragment : Fragment() {

    private lateinit var userProfile: UserProfile
    private lateinit var userImage: Bitmap
    private lateinit var editUser: MenuItem
    private var fromOnsale: Boolean = false

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyReviewsOnProfileAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    var userReviews = MutableLiveData<List<Review>>()
    lateinit var reviews: List<Review>

    private lateinit var googleMap: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_show_profile, container, false)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        val calleeBundle: Bundle? = this.arguments //Retrieve the callee from the bundle and check its value -> if true, no edit should be inflated
        if(calleeBundle != null && calleeBundle.containsKey("fromOnSale")) {
            val isFromOnSale: Boolean = calleeBundle.getBoolean("fromOnSale")
            if (!isFromOnSale) {
                inflater.inflate(R.menu.profile_menu, menu)
                editUser = menu.findItem(R.id.edit_user)
                if(this::userProfile.isInitialized)
                    editUser.isEnabled = true
            }
        }
        else {
            inflater.inflate(R.menu.profile_menu, menu)
            editUser = menu.findItem(R.id.edit_user)
            if(this::userProfile.isInitialized)
                editUser.isEnabled = true
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.edit_user -> {
                editProfile()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val calleeBundle: Bundle? = this.arguments //Retrieve the callee from the bundle and check its value -> if true, no edit should be inflated
        if(calleeBundle != null) {
            fromOnsale = calleeBundle.getBoolean("fromOnSale")
            if (fromOnsale) {
                requireActivity().nav_view.visibility = View.INVISIBLE
            }
        }

        hideViewsForLoading()
        showProgressBar()

        val item:             Item?   = arguments?.getParcelable("item")
        val guestUserId:      String? = arguments?.getString("userGuestId")
        val guestUser:   UserProfile? = arguments?.getParcelable("userGuestProfile")
        val firebaseUserMail: String? = item?.sellerEmail ?: Firebase.auth.currentUser!!.email

        val updatedUser: UserProfile? = calleeBundle?.getParcelable("userProfileUpdated")

        // Commodity variable for keeping track of the user
        var finalUser: UserProfile? = null

        if(fromOnsale) {
            if(guestUser != null) {
                val imageRepo = ViewModelProvider(this).get(ImageRepository::class.java)
                val image     = imageRepo.getUserImage(guestUser.photoId)
                image.observe(viewLifecycleOwner, Observer { userImage: Bitmap? ->
                    populateXml(guestUser, userImage, fromOnsale, savedInstanceState)
                })
                finalUser = guestUser
            } else {
                val userRepo = ViewModelProvider(this).get(UserFirebaseViewModel::class.java)
                val userId = item?.sellerId ?: guestUserId!!
                userRepo.getSavedUser(userId)
                    .observe(viewLifecycleOwner, Observer { guestUser: UserProfile ->
                        val imageRepo = ViewModelProvider(this).get(ImageRepository::class.java)
                        val image = imageRepo.getUserImage(guestUser.photoId)
                        image.observe(viewLifecycleOwner, Observer { userImage: Bitmap? ->
                            populateXml(guestUser, userImage, fromOnsale, savedInstanceState)
                        })
                        finalUser = guestUser
                    })
            }
        } else if(updatedUser != null) {
            finalUser = updatedUser
            if(calleeBundle.containsKey("userImageUpdated")) {
                val imageBundle = readBitmapFromStorageMemory(updatedUser.photoId, requireContext())
                populateXml(updatedUser, imageBundle, fromOnsale, savedInstanceState)
            } else {
                val image     = ImageRepository().getUserImage(updatedUser.photoId)

                image.observe(viewLifecycleOwner, Observer { userImage: Bitmap? ->
                    populateXml(updatedUser, userImage, fromOnsale, savedInstanceState)
                })
            }
        } else {
            val savedUser = UserFirebaseViewModel().getSavedUserByEmail(firebaseUserMail!!)
            savedUser.observe(viewLifecycleOwner, Observer { userChanged: UserProfile ->
                val imageRepo = ViewModelProvider(this).get(ImageRepository::class.java)
                val image     = imageRepo.getUserImage(userChanged.photoId)

                image.observe(viewLifecycleOwner, Observer { userImage: Bitmap? ->
                    populateXml(userChanged, userImage, fromOnsale, savedInstanceState)
                })
                finalUser = userChanged
            })
        }

        scrollView.setOnTouchListener { view, _ ->
            view.performClick()
            scrollView.requestDisallowInterceptTouchEvent(false)
            false
        }

        userProfileMapViewTransparent.setOnTouchListener { v, event ->
            v.performClick()
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    scrollView.requestDisallowInterceptTouchEvent(true)
                    false
                }
                MotionEvent.ACTION_UP -> {
                    scrollView.requestDisallowInterceptTouchEvent(false)
                    true
                }
                MotionEvent.ACTION_MOVE -> {
                    scrollView.requestDisallowInterceptTouchEvent(true)
                    false
                }
                else -> true
            }
        }
    }


    private fun populateXml(user: UserProfile, image: Bitmap?, fromOnSale: Boolean, savedInstanceState: Bundle?) {

        userProfile = user

        userProfileName.text            = user.name
        userProfileNickname.text        = user.nickname
        userProfileEmail.text           = user.email
        userProfileLocation.text        = user.location
        userProfileStateLocation.text   = user.country
        userProfileOverallRating.rating = user.overallRating.toFloat()
        reviewsCounter.text             = "(${user.nReviews})"
        if(fromOnSale){
            userProfilePrefix.text = ""
            userProfilePhone.text = ""

        } else {
            userProfilePrefix.text = user.prefix
            userProfilePhone.text = user.phone
        }

        // Map management
        if(userProfile.latitude != null && userProfile.longitude != null && userProfile.latitude != 0.0 && userProfile.longitude != 0.0) {
            // Map management
            userProfileMapView.visibility = View.VISIBLE
            userProfileMapViewTransparent.visibility = View.VISIBLE
            userProfileMapView.onCreate(savedInstanceState)
            userProfileMapView.onResume()
            userProfileMapView.getMapAsync {
                googleMap = it
                googleMap.uiSettings.isZoomControlsEnabled = true
                googleMap.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            userProfile.latitude,
                            userProfile.longitude
                        )
                    )
                )
                googleMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            userProfile.latitude,
                            userProfile.longitude
                        ), 16.0f
                    )
                ) // Moving map to the user's location
            }
        }

        image?.let {
            userImage = cropAndResizeBitmap(it, screenAvailableSize(requireContext()))
            userProfileImage.setImageBitmap(userImage)

            if(!fileExist(userProfile.photoId, requireContext(), false))
                DoAsync{
                    writeBitmapToStorageMemory(userImage, openFile(userProfile.photoId, requireActivity()))
                }.execute()

        }

        hideProgressBar()
        restoreViewsAfterLoading()
        populateUserReviews(userProfile)
    }

    private fun editProfile() {
        // Retrieve current user

        val bundle = Bundle()

        bundle.putParcelable("userProfile", userProfile)
        if(this::userImage.isInitialized) {
            bundle.putBoolean("userProfileImage", true)
        }

        findNavController().navigate(R.id.nav_edit_profile, bundle)

    }

    private fun populateUserReviews(user: UserProfile) {
        showProgessBarUserReviews()

        val reviewRepo = ViewModelProvider(this).get(ReviewViewModel::class.java)
        reviewRepo.getReviewsByUserId(user.id).observe(viewLifecycleOwner, Observer { userReviewsDb ->
            userReviews.value = userReviewsDb.filter { review -> review.text != "" }
            reviews = userReviewsDb

            viewManager = LinearLayoutManager(requireContext())
            viewAdapter = MyReviewsOnProfileAdapter(
                userReviews,
                viewLifecycleOwner,
                object : ContextProvider {
                    override fun getContext(): Context {
                        return activity!!.applicationContext
                    }
                }
            )
            recyclerView =
                requireView().findViewById<RecyclerView>(R.id.userProfileReviewsRecyclerView)
                    .apply {
                        // use a linear layout manager
                        layoutManager = viewManager
                        // specify an viewAdapter (see also next example)
                        adapter = viewAdapter
                    }

            if(userReviews.value.isNullOrEmpty()) {
                userProfileEmptyList.visibility = View.VISIBLE
                userProfileReviewsRecyclerView.visibility = View.GONE
            } else {
                userProfileEmptyList.visibility = View.INVISIBLE
                userProfileReviewsRecyclerView.visibility = View.VISIBLE
            }

            hideProgessBarUserReviews()

        })
    }

    private fun showProgressBar() {
        progressBarProfile.visibility=View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBarProfile.visibility=View.INVISIBLE
    }

    private fun showProgessBarUserReviews() {
        progressBarUserReviews.visibility = View.VISIBLE
    }

    private fun hideProgessBarUserReviews() {
        progressBarUserReviews.visibility = View.INVISIBLE
    }

    private fun hideViewsForLoading() {
        userProfileName.visibility = View.INVISIBLE
        userProfileNameLabel.visibility = View.INVISIBLE
        userProfileNickname.visibility = View.INVISIBLE
        userProfileNicknameLabel.visibility = View.INVISIBLE
        userProfileEmail.visibility = View.INVISIBLE
        userProfileLocation.visibility = View.INVISIBLE
        userProfileLocationCityLabel.visibility = View.INVISIBLE
        userProfileStateLocation.visibility = View.INVISIBLE
        userProfileLocationStateLabel.visibility = View.INVISIBLE
        userProfilePrefix.visibility = View.GONE
        userProfilePhone.visibility = View.GONE
        userProfilePhoneLabel.visibility = View.GONE
        userProfileImage.visibility = View.INVISIBLE
        reviewsSection.visibility = View.INVISIBLE
        profileSection.visibility = View.INVISIBLE
        locationSection.visibility = View.INVISIBLE
        userProfileOverallRating.visibility = View.INVISIBLE
        reviewsCounter.visibility = View.INVISIBLE
        userProfileMapViewContainer.visibility = View.GONE

        if(this::editUser.isInitialized)
            editUser.isEnabled = false
    }

    private fun restoreViewsAfterLoading() {
        userProfileName.visibility = View.VISIBLE
        userProfileNameLabel.visibility = View.VISIBLE
        userProfileNickname.visibility = View.VISIBLE
        userProfileNicknameLabel.visibility = View.VISIBLE
        userProfileEmail.visibility = View.VISIBLE
        userProfileLocation.visibility = View.VISIBLE
        userProfileLocationCityLabel.visibility = View.VISIBLE
        userProfileStateLocation.visibility = View.VISIBLE
        userProfileLocationStateLabel.visibility = View.VISIBLE

        if (!fromOnsale) {
            userProfilePrefix.visibility = View.VISIBLE
            userProfilePhone.visibility = View.VISIBLE
            userProfilePhoneLabel.visibility = View.VISIBLE
        }
        userProfileImage.visibility = View.VISIBLE
        reviewsSection.visibility = View.VISIBLE
        profileSection.visibility = View.VISIBLE
        locationSection.visibility = View.VISIBLE
        userProfileOverallRating.visibility = View.VISIBLE
        reviewsCounter.visibility = View.VISIBLE
        userProfileMapViewContainer.visibility = View.VISIBLE

        if(this::editUser.isInitialized)
            editUser.isEnabled = true

    }
}