package com.mad2020.group09.buybye.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.model.Review
import com.mad2020.group09.buybye.repositories.ImageRepository
import com.mad2020.group09.buybye.repositories.UserFirebaseViewModel
import com.mad2020.group09.buybye.utilities.*
import kotlinx.android.synthetic.main.relative_layout_user_profile_reviews.view.*

class MyReviewsOnProfileAdapter(
    private val myReviewDataset: LiveData<List<Review>>,
    private val lifecycleOwner: LifecycleOwner,
    private val myContextProvider: ContextProvider
) :
    LifecycleRecyclerAdapter<MyReviewsOnProfileAdapter.MyViewHolder>() {

    class MyViewHolder(private val reviewView: ViewGroup) : LifecycleViewHolder(reviewView) {
        fun bindUser(
            review: Review,
            lifecycleOwner: LifecycleOwner,
            contextProvider: ContextProvider
        ) {

            showProgressBar()

            reviewView.reviewUserProfileText.text = review.text
            reviewView.reviewUserProfileRating.rating = review.rating.toFloat()

            UserFirebaseViewModel().getSavedUser(review.userReviewerId)
                .observe(lifecycleOwner, Observer { reviewer ->

                    reviewView.reviewUserProfileName.text = reviewer.name

                    if (fileExist(reviewer.photoId, contextProvider.getContext(), true)) {
                        hideProgressBar()
                        reviewView.reviewUserProfileImage.setImageBitmap(
                            cropAndResizeBitmap(
                                readBitmapFromStorageMemory(
                                    reviewer.photoId,
                                    contextProvider.getContext()
                                )!!,
                                100
                            )
                        )
                        reviewView.reviewUserProfileImage.visibility = View.VISIBLE
                    } else {
                        ImageRepository().getUserImage(reviewer.photoId)
                            .observe(lifecycleOwner, Observer {
                                hideProgressBar()
                                if (it != null) {
                                    reviewView.reviewUserProfileImage.setImageBitmap(
                                        cropAndResizeBitmap(it, 100)
                                    )
                                    writeBitmapToStorageMemory(
                                        it, openFile(
                                            reviewer.photoId,
                                            contextProvider.getContext()
                                        )
                                    )
                                } else {
                                    reviewView.reviewUserProfileImage.setImageResource(R.drawable.my_avatar)
                                }
                                reviewView.reviewUserProfileImage.visibility = View.VISIBLE
                            })
                    }
                })

        }

        //
        private fun showProgressBar() {
            reviewView.progressBarReview.visibility = View.VISIBLE
        }

        private fun hideProgressBar() {
            reviewView.progressBarReview.visibility = View.INVISIBLE
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        // create a new view
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.relative_layout_user_profile_reviews, parent, false) as ViewGroup

        return MyViewHolder(
            layout
        )
    }

    override fun getItemCount(): Int {
        return myReviewDataset.value!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val review: Review = myReviewDataset.value!![position]

        holder.bindUser(review, lifecycleOwner, myContextProvider)
    }
}