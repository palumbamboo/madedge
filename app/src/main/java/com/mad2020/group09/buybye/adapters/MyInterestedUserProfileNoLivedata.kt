package com.mad2020.group09.buybye.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.mad2020.group09.buybye.R
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.utilities.cropAndResizeBitmap
import com.mad2020.group09.buybye.utilities.fileExist
import com.mad2020.group09.buybye.utilities.readBitmapFromStorageMemory
import kotlinx.android.synthetic.main.fragment_user_interested_card.view.*


class MyInterestedUserProfileNoLivedata(
    private val myUserDataset: List<UserProfile>,
    private val myContextProvider: ContextProvider,
    private val selectedUser: MutableLiveData<UserProfile>
) : RecyclerView.Adapter<MyInterestedUserProfileNoLivedata.MyViewHolder>() {

    class MyViewHolder(private val cardView: CardView) : RecyclerView.ViewHolder(cardView) {

        fun bindUser(
            user: UserProfile,
            contextProvider: ContextProvider,
            selectedUser: MutableLiveData<UserProfile>
        ) {
            cardView.cardUserProfileImage.visibility = View.INVISIBLE
            cardView.cardUserProfileName.text = user.name
            cardView.cardUserProfileLocation.text = user.location
            cardView.cardUserProfileRating.rating = user.overallRating.toFloat()

            if (fileExist(user.photoId, contextProvider.getContext(), false)) {
                cardView.cardUserProfileImage.setImageBitmap(
                    cropAndResizeBitmap(
                        readBitmapFromStorageMemory(
                            user.photoId,
                            contextProvider.getContext()
                        )!!,
                        150
                    )
                )
            }
            cardView.cardUserProfileImage.visibility = View.VISIBLE

            cardView.setOnClickListener {
                androidx.appcompat.app.AlertDialog
                    .Builder(contextProvider.getContext())
                    .setMessage("Confirm buyer?")
                    .setPositiveButton("Confirm") { dialog, which ->
                        selectedUser.value = user
                        Toast.makeText(contextProvider.getContext(), "Buyer selected", Toast.LENGTH_SHORT).show()
                    }.setNegativeButton("Back") { dialog, which ->
                        dialog.dismiss()
                    }.show()
            }
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        // create a new view
        val card = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_user_interested_card, parent, false) as MaterialCardView

        return MyViewHolder(
            card
        )
    }

    override fun getItemCount(): Int {
        return myUserDataset.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val user: UserProfile = myUserDataset[position]

        holder.bindUser(user, myContextProvider, selectedUser)
    }
}