package com.mad2020.group09.buybye.ui.items

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.mad2020.group09.buybye.R
import android.view.Menu as Menu1
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.mad2020.group09.buybye.adapters.ContextProvider
import com.mad2020.group09.buybye.adapters.ItemsOfInterestAdapter
import com.mad2020.group09.buybye.model.Item
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.InterestViewModel
import com.mad2020.group09.buybye.repositories.ItemFirebaseViewModel
import com.mad2020.group09.buybye.enums.ItemStatus
import com.mad2020.group09.buybye.enums.SortOption
import com.mad2020.group09.buybye.ui.dialogs.SortDialog
import com.mad2020.group09.buybye.ui.home.OnSaleListFragment
import com.mad2020.group09.buybye.utilities.getCurrentUser
import kotlinx.android.synthetic.main.fragment_items_of_interest_list.*
import kotlinx.android.synthetic.main.fragment_on_sale_list.*


class ItemsOfInterestListFragment : Fragment(), SortDialog.DialogListener {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: ItemsOfInterestAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var searchView: SearchView
    private lateinit var userProfile: UserProfile

    var data = MutableLiveData<List<Item>>()
    lateinit var da: List<Item>
    private var sort: String = SortOption.DATE.option

    companion object {
        const val DIALOG_FRAGMENT_SORT = 2
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_items_of_interest_list, container, false)
    }


    override fun onCreateOptionsMenu(menu: Menu1, inflater: MenuInflater) {
        inflater.inflate(R.menu.customer_list_menu, menu) // same menu as ItemListFragment
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setOnCloseListener { false }

        val searchPlate =
            searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
        searchPlate.hint = "Search" // Searching by title, category, subcategory or location
        val searchPlateView: View = searchView.findViewById(androidx.appcompat.R.id.search_plate)
        searchPlateView.setBackgroundColor(
            ContextCompat.getColor(
                requireActivity().applicationContext,
                android.R.color.transparent
            )
        )

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                // method called after the user clicks the enter/search after inputting the search query

                viewAdapter.filter.filter(query)
                if (viewAdapter.itemCount == 0)
                    Toast.makeText(requireContext(), "No match found.", Toast.LENGTH_SHORT).show()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // method used when the change occurs in the query field

                viewAdapter.filter.filter(newText)

                return false
            }
        })

        val sortItem: MenuItem = menu.findItem(R.id.sort_tune)
        sortItem.setOnMenuItemClickListener {
            val fragmentTransaction = parentFragmentManager.beginTransaction()
            val prev = parentFragmentManager.findFragmentByTag("dialog")
            if (prev != null) {
                fragmentTransaction.remove(prev)
            }

            val tuneFragment = SortDialog()
            tuneFragment.setTargetFragment(this, DIALOG_FRAGMENT_SORT)

            val bundle = Bundle()
            bundle.putString("sort", sort)
            tuneFragment.arguments = bundle

            tuneFragment.show(fragmentTransaction, "dialog")

            true
        }

        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (sort != SortOption.DATE.option) {
            outState.putString("sortOption", sort)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var tmpSort = savedInstanceState?.getString("sortOption")

        if (tmpSort != null && tmpSort!= SortOption.DATE.option) {
            sort = tmpSort
        }

        txtEmptyListInterestList.visibility = View.INVISIBLE
        getItemsFromDB()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            DIALOG_FRAGMENT_SORT -> {
                if (resultCode == Activity.RESULT_OK) {

                    reloadSortedItem(sort)

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // After Cancel code.
                }
            }
        }
    }

    private fun getItemsFromDB() {
        showProgressBar()
        val itemRepo = ViewModelProvider(this).get(ItemFirebaseViewModel::class.java)
        val interestRepo = ViewModelProvider(this).get(InterestViewModel::class.java)

        getCurrentUser().observe(viewLifecycleOwner, Observer { currentUser ->
            userProfile = currentUser!!
            interestRepo.getInterestsForUser(userProfile.id)
                .observe(viewLifecycleOwner, Observer { itemIds ->

                    if (itemIds.isNullOrEmpty()) {
                        txtEmptyListInterestList.visibility = View.VISIBLE
                        items_of_interest_recycler_view.visibility = View.GONE
                        hideProgressBar()
                        return@Observer
                    }

                    itemRepo.getSavedItemsByIds(itemIds.map { it.itemId })
                        .observe(viewLifecycleOwner, Observer { interestItems ->
                            data.value = interestItems.sortedBy { item -> item.creationDate }.reversed()
                            when (sort) {
                                SortOption.LOWERPRICE.option -> data.value = interestItems.sortedBy { item -> item.price }
                                SortOption.HIGHERPRICE.option -> data.value = interestItems.sortedBy { item -> item.price }.reversed()
                            }
                            da = interestItems!!

                            data.value =
                                data.value!!.filter { item -> item.status == ItemStatus.ON_SALE.status }

                            viewManager = LinearLayoutManager(this.context)
                            viewAdapter = ItemsOfInterestAdapter(
                                data,
                                userProfile,
                                viewLifecycleOwner,
                                object : ContextProvider {
                                    override fun getContext(): Context {
                                        return activity!!.applicationContext
                                    }
                                }
                            )

                            recyclerView =
                                requireView().findViewById<RecyclerView>(R.id.items_of_interest_recycler_view)
                                    .apply {
                                        // use a linear layout manager
                                        layoutManager = viewManager
                                        // specify an viewAdapter (see also next example)
                                        adapter = viewAdapter
                                    }

                            if (data.value.isNullOrEmpty()) {
                                txtEmptyListInterestList.visibility = View.VISIBLE
                                recyclerView.visibility = View.GONE
                            } else {
                                txtEmptyListInterestList.visibility = View.INVISIBLE
                                recyclerView.visibility = View.VISIBLE
                            }

                            hideProgressBar()
                        })
                })
        })
    }

    override fun onFinishEditSortDialog(sort: String) {
        this.sort = sort
    }

    private fun reloadSortedItem(sort: String) {
        when (sort) {
            SortOption.DATE.option -> {
                viewAdapter.filterItemDataset.value =
                    viewAdapter.filterItemDataset.value!!.sortedBy { item -> item.creationDate }
                        .reversed()
            }
            SortOption.LOWERPRICE.option -> {
                viewAdapter.filterItemDataset.value =
                    viewAdapter.filterItemDataset.value!!.sortedBy { item -> item.price }
            }
            SortOption.HIGHERPRICE.option -> {
                viewAdapter.filterItemDataset.value =
                    viewAdapter.filterItemDataset.value!!.sortedBy { item -> item.price }.reversed()
            }
        }
        viewAdapter.notifyDataSetChanged()
    }

    private fun showProgressBar() {
        progressItemsOfInterestList.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressItemsOfInterestList.visibility = View.INVISIBLE
    }

}
