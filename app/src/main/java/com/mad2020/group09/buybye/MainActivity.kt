package com.mad2020.group09.buybye

import android.graphics.Bitmap
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.mad2020.group09.buybye.model.UserProfile
import com.mad2020.group09.buybye.repositories.ImageRepository
import com.mad2020.group09.buybye.repositories.UserFirebaseViewModel
import com.mad2020.group09.buybye.utilities.cropAndResizeBitmap
import com.mad2020.group09.buybye.utilities.fileExist
import com.mad2020.group09.buybye.utilities.readBitmapFromStorageMemory

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_myList,
                R.id.nav_itemsOfInterest,
                R.id.nav_boughtItemsList,
                R.id.nav_itemsToConfirm,
                R.id.nav_profile,
                R.id.nav_login
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        val headerView = navView.getHeaderView(0)
        if (Firebase.auth.currentUser != null) {
            navView.menu.findItem(R.id.nav_login).title = "Logout"
            navView.menu.findItem(R.id.nav_login).setIcon(R.drawable.logout)

            Firebase.auth.currentUser!!.email?.let {
                UserFirebaseViewModel().getSavedUserByEmail(
                    it
                )
            }?.observe(this, Observer { currentUser: UserProfile ->

                if (currentUser.nickname != "") {
                    headerView.findViewById<TextView>(R.id.headerTitle).text =
                        currentUser.nickname
                }
                headerView.findViewById<TextView>(R.id.headerDescription).text = currentUser.email

                if (fileExist(currentUser.photoId, this, true)) {
                    headerView.findViewById<ImageView>(R.id.headerImage).setImageBitmap(
                        readBitmapFromStorageMemory(currentUser.photoId, this)?.let {
                            cropAndResizeBitmap(
                                it, 208
                            )
                        })
                } else {

                    val imageRepo = ViewModelProvider(this).get(ImageRepository::class.java)
                    val image = imageRepo.getUserImage(currentUser.photoId)

                    image.observe(this, Observer { userImage: Bitmap? ->

                        if (userImage != null) {
                            userImage.let {
                                headerView.findViewById<ImageView>(R.id.headerImage)
                                    .setImageBitmap(
                                        cropAndResizeBitmap(
                                            userImage,
                                            208
                                        )
                                    )

                            }
                        } else {
                            headerView.findViewById<ImageView>(R.id.headerImage).setImageBitmap(
                                cropAndResizeBitmap(
                                    ContextCompat.getDrawable(
                                        this,
                                        R.drawable.my_avatar
                                    )!!.toBitmap(), 70
                                )
                            )
                        }
                    })
                }
            })
        } else {
            // User not authenticated
            navView.menu.findItem(R.id.nav_myList).isVisible = false
            navView.menu.findItem(R.id.nav_profile).isVisible = false
            navView.menu.findItem(R.id.nav_itemsOfInterest).isVisible = false
            navView.menu.findItem(R.id.nav_boughtItemsList).isVisible = false
            navView.menu.findItem(R.id.nav_itemsToConfirm).isVisible = false
            navView.menu.findItem(R.id.nav_login).title = "Login"
            navView.menu.findItem(R.id.nav_login).setIcon(R.drawable.login)


            headerView.findViewById<TextView>(R.id.headerTitle).text = "BuyBye"
            headerView.findViewById<TextView>(R.id.headerDescription).text =
                "Buy first, Bye next!"
            headerView.findViewById<ImageView>(R.id.headerImage)
                .setImageResource(R.mipmap.ic_launcher_round)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}
