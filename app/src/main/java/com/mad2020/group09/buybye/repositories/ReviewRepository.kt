package com.mad2020.group09.buybye.repositories

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.mad2020.group09.buybye.model.Review

class ReviewRepository {
    companion object {
        private const val TAG = "REVIEW_FIREBASE_REPOSITORY"
        private const val REVIEW_REPO = "Reviews"
    }

    private val firestoreDB = FirebaseFirestore.getInstance()

    // Save a review on the collection
    fun saveReview(review: Review): Task<Void> {
        val interestReference = firestoreDB.collection(REVIEW_REPO).document(review.id)
        Log.d(TAG, "Added review ${review.id} to reviews repo")
        return interestReference.set(review)
    }

    // Delete a review
    fun deleteReview(review: Review): Task<Void> {
        return firestoreDB.collection(REVIEW_REPO).document(review.id).delete()
    }

    fun getReviewsByUserId(userId: String): Query {
        return firestoreDB.collection(REVIEW_REPO).whereEqualTo("userReviewedId", userId)
    }

    fun getReviewByUserAndItem(userId: String, itemId: String): Query {
        return firestoreDB.collection(REVIEW_REPO).whereEqualTo("userReviewerId", userId)
            .whereEqualTo("itemReviewedId", itemId)
    }

}